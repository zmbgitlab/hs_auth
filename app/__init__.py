from huansi_utils.webapi.HSWebApi import HSWebApi
from huansi_utils.common.modeloader import dynamic_import

_webApi = HSWebApi("api", __name__)

# 导出简单的类装饰器
api = _webApi.api

# dynamic_import(import_prefix=_webApi.import_name, root_dir=_webApi.root_path, file_suffix="_api.py")
dynamic_import(import_prefix=_webApi.import_name, root_dir=_webApi.root_path, file_suffix=["_api.py","_api.pyc"])
