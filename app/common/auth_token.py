#!/usr/bin/python
# -*- coding: utf-8 -*-
import time
import base64
import hmac
import hashlib
import random


def safe_auth_token(ba64_token):
    if len(ba64_token) % 4 != 0:
        ba64_token = ba64_token[:-2]
    return ba64_token


def time_stamp_str(expire_in=15 * 60):
    # 生成时间戳
    return str(time.time() + expire_in)


def verify_time_stamp(time_stamp):
    # 校验时间戳
    if float(time_stamp) > time.time():
        return True
    return False


def gen_sha1auth_token(time_stamp, sha1_tshexstr):
    # sha1_tshexstr和时间戳绑定
    return time_stamp + ':' + sha1_tshexstr


def gen_sha1_str(secret_key, time_stamp):
    # 生成用于token的sha1_str
    return hmac.new(secret_key.encode("utf-8"), time_stamp.encode("utf-8"), digestmod=hashlib.sha1).hexdigest()


def encode_sha1_str(sha1auth_token):
    # 编码sha1_str
    return base64.b64encode(sha1auth_token.encode("utf-8")).decode("utf-8")


def decode_sha1_str(auth_token):
    # 解码sha1_str
    auth_token_str = base64.b64decode(auth_token).decode('utf-8')
    auth_token_list = auth_token_str.split(':')
    if len(auth_token_list) != 2:
        raise RuntimeError('auth_token格式不合法')
    time_stamp = auth_token_list[0]
    known_sha1_tsstr = auth_token_list[1]
    return time_stamp, known_sha1_tsstr


def generate_auth_token(grant_type, secret_key=None, app_id=None, refresh_token=None,
                        expire_in=120 * 60):
    '''生成 auth_token
    :param secret_key: 自定义的key,用于生成token
    :param expire_in: token的有效时间(单位:秒)
    :return: auth_token, expire_in
    auth_token生成原理: 通过hmac,sha1把处理后的byte类型的有效期和自定义参数secret_key,生成一个消息摘要,
    把消息摘要与有效期expire_in以 ':' 拼接,最后用base64编码,生成auth_toke。
    '''
    if grant_type == 'access_token' and secret_key:
        print('gen_secret_key', secret_key)
        time_stamp = time_stamp_str(expire_in)
        sha1_tshexstr = gen_sha1_str(secret_key, time_stamp)
        sha1auth_token = gen_sha1auth_token(time_stamp, sha1_tshexstr)
        auth_token = encode_sha1_str(sha1auth_token)
        # auth_token = safe_auth_token(auth_token)
        return auth_token, time_stamp
    elif grant_type == 'refresh_token' and refresh_token and app_id:
        old_time_stamp, known_sha1_tsstr = decode_sha1_str(auth_token=refresh_token)
        if verify_time_stamp(old_time_stamp):
            time_stamp = time_stamp_str(expire_in)
            sha1_tshexstr = gen_sha1_str(app_id, time_stamp)
            sha1auth_token = gen_sha1auth_token(time_stamp, sha1_tshexstr)
            auth_token = encode_sha1_str(sha1auth_token)
            return auth_token, time_stamp
        raise RuntimeError('Time out no refresh token')

    else:
        raise RuntimeError('Invalid generate token type')


def verify_auth_token(secret_key=None, auth_token=None):
    '''验证 auth_token
    :param secret_key: 自定义的key
    :param auth_token: token_gen_verify
    :return: true / false
    auth_token的验证原理: 将auth_token进行base64解码,获取token的有效期和消息摘要,
    然后判断token是否过期, 在有效期内的话,从取出token的有效期与自定义的secret,
    通过hmac,sha1把处理后的byte类型的有效期和自定义的secret_key,生成一个消息摘要,
    如果在有效期内, 两个摘要又相等，则auth_token验证通过，否则auth_token失效。
    '''
    if secret_key and auth_token:
        time_stamp, known_sha1_tsstr = decode_sha1_str(auth_token)
        if verify_time_stamp(time_stamp):
            calc_sha1_tsstr = gen_sha1_str(secret_key, time_stamp)
            if calc_sha1_tsstr == known_sha1_tsstr:
                return True
            raise RuntimeError('Auth_token not equal')
        raise RuntimeError('Invalid Auth_token')
    else:
        raise RuntimeError('Verify auth_token params error')


def get_random_string(chars='', length=12):
    '''生成12位的随机数字字符串
    :param length: 长度
    :param chars: 可变参数
    :return: 返回随机字符串,默认长度为12，由自定义字符串参数和包含71位的 a-z,A-Z,0-9字符集值。算法:log_2 ((26+26+10)^12) = ~71位
    '''
    """
    Returns a securely generated random string.
    The default length of 12 with the a-z, A-Z, 0-9 character set returns
    a 71-bit value. log_2((26+26+10)^12) =~ 71 bits
    """
    secure_chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    # 使用字符串函数join拼接,该方法多用于列表，元组，对较大的字符串拼接操作
    allowed_chars = ''.join((secure_chars, chars))
    return ''.join(random.choice(allowed_chars) for i in range(length))


def get_verify_code(digits=6):
    '''生成code,可用于auth_code, message_code
    :param digits: code的位数
    :return:
    '''
    chars = '0123456789'
    six_num = [random.choice(chars) for i in range(digits)]
    verify_code = ''.join(six_num)
    return verify_code


def gen_token_key():
    '''
    随机生成数字和字母组合的25位字符串
    :return: key
    '''
    key = ''
    for a in range(97, 123):
        b = chr(a)
        key += b
        key += b.upper()
    for c in range(0, 10):
        key += str(c)
    key2 = random.sample(key, 25)
    key3 = ''.join(key2)
    return key3
