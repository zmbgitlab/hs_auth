#!/usr/bin/python
# -*- coding: utf-8 -*-¬
from functools import wraps

'''简单装饰器'''


def user_logging(func):
    def wrapper():
        print('%s is running' % func.__name__)
        # 把foo函数当做参数传递进来, 执行func()就相当于执行foo()
        return func()

    return wrapper


def foo():
    print(11111, 'I am foo func')


# 因为装饰器 user_logging(foo),返回的是函数对象wrapper, 这条语句相当于 foo = wrapper
test1 = user_logging(foo)
# test1()就相当于执行 wrapper()
test1()

# use_logging 就是一个装饰器,它一个普通的函数,它把执行真正业务逻辑的函数 func 包裹在其中,看起来像 foo 被 use_logging 装饰了一样,
# use_logging 返回的也是一个函数,这个函数的名字叫 wrapper, 然后func函数是在函数wrapper里执行的。
# 这种例子在函数进入和退出时,被称为一个横切面,这种编程方式被称为面向切面的编程。

'''语法糖'''


# '@'符号就是装饰器的语法糖,它放在函数开始定义的地方,这样就可以省略最后一步'test1 = user_logging(foo)'再次赋值的操作。
# 有了 '@' 我们就可以省去 test1 = use_logging(foo)这一句了，直接调用 foo2() 即可得到想要的结果。
@user_logging
def foo2():
    print('@语法糖, I am foo2 func\n')


foo2()

# 装饰器在 Python 使用如此方便都要归因于 Python 的函数能像普通的对象一样能作为参数传递给其他函数,
# 可以被赋值给其他变量,可以作为返回值,可以被定义在另外一个函数内。

'''带参数的foo3的装饰器'''


# *args、**kwargs

def user_logging2(func):
    # 定义 wrapper 函数的时候指定参数
    def wrapper(*args, **kwargs):
        # args是一个数组,kwargs一个字典
        print('%s is running' % func.__name__)
        # 把foo函数当做参数传递进来, 执行func()就相当于执行foo()
        print('args', args)
        print('kwargs', kwargs)
        return func(*args, **kwargs)

    return wrapper


@user_logging2
def foo3(*args, **kwargs):
    print('i am args', args)
    print('i am kwargs', kwargs)


foo3(1, 2, name='张三')

'''带参数的装饰器'''


# 装饰器还有更大的灵活性,例如带参数的装饰器,在上面的装饰器调用中,该装饰器接收唯一的参数就是执行业务的函数 foo3
# 装饰器的语法允许我们在调用时,提供其它参数,比如 @user_logging2(a)
# 这样,就为装饰器的编写和使用提供了更大的灵活性。比如,我们可以在装饰器中指定日志的等级,因为不同业务函数可能需要的日志级别是不一样的。

def user_logging3(level):
    def decorator(func):
        # 定义 wrapper 函数的时候指定参数
        def wrapper(*args, **kwargs):
            if level == "warn":
                print("\n %s is running warn" % func.__name__)
            elif level == "info":
                print("\n %s is running info" % func.__name__)
            return func(*args, **kwargs)

        return wrapper

    return decorator


@user_logging3(level='info')
def foo4(*args, **kwargs):
    print('i am args', args)
    print('i am kwargs', kwargs)


foo4(245, name='李四')

# 上面的 use_logging 是允许带参数的装饰器。它实际上是对原有装饰器的一个函数封装,并返回一个装饰器,可以将它理解为一个含有参数的闭包。
# 当我 们使用@use_logging(level="warn")调用的时候,Python 能够发现这一层的封装，并把参数传递到装饰器的环境中。
# @user_logging3(level='info') 等价于 @decorator


'''类装饰器'''


# 装饰器不仅可以是函数，还可以是类，相比函数装饰器，类装饰器具有灵活度大、高内聚、封装性等优点。
# 使用类装饰器主要依靠类的__call__方法，当使用 @ 形式将装饰器附加到函数上时，就会调用此方法

class Foo(object):
    def __init__(self, func):
        self._func = func

    def __call__(self, *args, **kwargs):
        print('\n class decorator runing')
        self._func()
        print('class decorator ending')


@Foo
def bar():
    print('bar is runing')


bar()

'''functools.wraps'''


# 使用装饰器极大地复用了代码,但是他有一个缺点就是原函数的元信息不见了,比如函数的docstring、__name__、参数列表
# 先看下例子

# 函数 foo5 被with_logging取代了,当然它的docstring,__name__就是变成了with_logging函数的信息了。
# 好在我们有functools.wraps,wraps本身也是一个装饰器,它能把原函数的元信息拷贝到装饰器里面的 func 函数中,
# 这使得装饰器里面的 func 函数也有和原函数 foo 一样的元信息了。

def logged2(func):
    @wraps(func)
    def with_logging(*args, **kwargs):
        # 输出 'with_logging'
        print('\n 777', func.__name__)
        # 输出 None
        print('888', func.__doc__)
        return func(*args, **kwargs)

    return with_logging


@logged2
def foo6(x):
    """does some math"""
    return x + x * x


a = logged2(foo6)
print(a(4))

'''装饰器顺序'''


# @a
# @b
# @c
# def f ():
#     pass

# 它的执行顺序是从里到外,最先调用最里层的装饰器,最后调用最外层的装饰器,它等效于 f =a(b(c(f)))

# 以下是整理的通用装饰器

def user_logging8(level):
    def decorator(func):
        # 定义 wrapper 函数的时候指定参数
        @wraps(func)
        def wrapper(*args, **kwargs):
            if level == "warn":
                print("\n %s is running warn" % func.__name__)
                print('999', func.__name__)
                print('888', func.__doc__)
            elif level == "info":
                print("\n %s is running info" % func.__name__)
                print('999', func.__name__)
                print('888', func.__doc__)
            return func(*args, **kwargs)

        return wrapper

    return decorator


@user_logging8(level='info')
def foo8(*args, **kwargs):
    print('i am args', args)
    print('i am kwargs', kwargs)


foo8(245, name='李四')


class Foo8(object):
    def __init__(self, func):
        self._func = func

    def __call__(self, *args, **kwargs):
        print('\n class decorator runing')
        self._func()
        print('class decorator ending')
        print('class decorator __name__', self._func.__name__)
        print('class decorator __doc__', self._func.__doc__)


@Foo8
def bar():
    """coci"""
    print('bar is runing')


bar()
