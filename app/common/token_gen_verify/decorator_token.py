#!/usr/bin/python
# -*- coding: utf-8 -*-¬
from flask import request, jsonify
import requests
from functools import wraps


# token认证装饰器
def token_required(func):
    @wraps(func)
    def decorator(*args, **kwargs):
        if request.method == 'GET':
            auth_token = request.args.get('auth_token', None)
            if auth_token == '' or auth_token is None:
                return jsonify({'message': 'Invalid auth_token'})
            # TODO:业务处理:根据auth_token取出用户secret_key_random_str
            print(444, auth_token)
            key_url = 'http://127.0.0.1:5000/gen_secret_key_random_str/'
            before_secret_key_random_str = requests.get(url=key_url,
                                                        params={'before_token': auth_token})
            secret_key_random_str = before_secret_key_random_str.text
            print(555, secret_key_random_str)
            # secret_key_random_str = '6BWC3EaKTw14'
            secret_key = secret_key_random_str[1:]
            try:
                from app.common.token_gen_verify import verify_auth_token
                if secret_key and auth_token:
                    verify_auth_token = verify_auth_token(secret_key, auth_token)
                    if verify_auth_token is True:
                        return func(*args, **kwargs)
            except ValueError as e:
                raise e
        return jsonify({'message': 'Verify failed'})

    return decorator


# 用户验证装饰器
def user_auth(func):
    def auth(request, *args, **kwargs):
        # TODO:查询用户是否存在
        # user_obj = User.objects.filter(username=request.session.get("username")).first()
        # if not user_obj:
        #     return redirect(reverse("UnloginAPI"))
        return func(request, *args, **kwargs)

    return auth
