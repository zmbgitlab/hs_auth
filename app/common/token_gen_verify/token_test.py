#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
from oauthlib.common import to_unicode
from flask import Flask, request, redirect, jsonify
from app.common.token_gen_verify import get_random_string, generate_auth_token, get_verify_code, \
    verify_auth_token
from app.common.token_gen_verify import token_required

app = Flask(__name__)


@app.route('/index/', methods=['GET'])
def index():
    return jsonify('hello')


'''密码模式'''


@app.route('/login_token/', methods=['GET', 'POST'])
def login_token():
    # 登录接口,并生成token

    if request.method == 'GET':
        username, password = to_unicode(base64.b64decode(request.headers['Authorization'].split(' ')[-1])).split(':')
    else:
        username = request.get_json().get('username', None)
        password = request.get_json().get('password', None)
    if username and password and username != '' and password != '':
        # 使用%或者format进行拼接,多个变量是倾向于使用这种方法，因为它的可读性更强,chars = '%s%s' % (username, password)
        chars = '{0}{1}'.format(username, password)
        secret_key_random_str = get_random_string(chars)
        print('secret_key_random_str:', secret_key_random_str)
        secret_key = secret_key_random_str[1:]
        auth_token, time_stamp = generate_auth_token(secret_key, expire=90)
        return jsonify({'auth_token': auth_token, 'time_stamp': time_stamp})
    return jsonify({'message': 'Invalid username or password'})


'''授权码模式登录'''

# 全局测试参数
users2 = {"tom": "123456"}
# 客户端ID
client_id2 = '123456'
# 保存客户ID到用户
users2[client_id2] = []
# 保存授权码和重定向url
auth_code2 = {}
# 保存重定向URL
oauth_redirect_url2 = []
# 重定向URL
redirect_url2 = 'http://localhost:5000/client_passport2/'
# 保存uid和secret_key_random_str
before_secret_key = {}


@app.route('/oauth2/', methods=['GET'])
def oauth2():
    # B.Authorization Server要向用户确认是否给client授权。方法是让用户向Authorization Server提供用户名和密码。
    if request.args.get('user'):
        if users2.get(request.args.get('user')) == request.args.get('pw') and oauth_redirect_url2:
            # C.Authorization Server收到用户的授权后,会重定向到client的URL,并附带Authorization code。
            code = get_verify_code()
            auth_code2[code] = oauth_redirect_url2[0]
            url = oauth_redirect_url2[0] + '?code=%s' % code
            return redirect(url)
    if request.args.get('code'):
        if auth_code2.get(request.args.get('code')) == request.args.get('redirect_url'):
            # E.Authorization Server验证Authorization code和URI之后，向client发送token。
            uid = request.args.get('client_id')
            secret_key_random_str = get_random_string(uid)
            secret_key = secret_key_random_str[1:]
            print('secret_key_random_str:', secret_key_random_str)
            print('secret_key', secret_key)
            auth_token = generate_auth_token(secret_key, expire=90)
            before_secret_key[auth_token] = secret_key_random_str
            return auth_token
    # 保存A过程的client将用户登陆重定向URL
    if request.args.get('redirect_url'):
        oauth_redirect_url2.append(request.args.get('redirect_url'))
    return 'please login'


@app.route('/client_login2/', methods=['GET'])
def client_login2():
    # A.用户登陆client程序。在用户登陆的时候,client将用户登陆重定向到Authorization Server,并附上client的ID和重定向URI
    url = 'http://localhost:5000/oauth2/?response_type=code&client_id=%s&redirect_url=%s' % (client_id2, redirect_url2)
    return redirect(url)


@app.route('/client_passport2/', methods=['GET'])
def client_passport2():
    # D.client使用Authorization code和URI向Authorization Server请求token。
    code = request.args.get('code')
    url = 'http://localhost:5000/oauth2/?grant_type=authorization_code&code=%s&redirect_url=%s&client_id=%s' % (
        code, redirect_url2, client_id2)
    return redirect(url)


@app.route('/test_login2/', methods=['GET'])
def test_login2():
    # 测试 auth_token, secret_key的有效性
    auth_token = request.args.get('auth_token')
    secret_key = request.args.get('secret_key')
    if verify_auth_token(secret_key, auth_token):
        return 'success'
    return 'error'


@app.route('/gen_secret_key_random_str/', methods=['GET', 'POST'])
def get_before_secret_key():
    before_token = request.args.get('before_token')
    print('before_secret_key', before_secret_key)
    print('before_token', before_token)
    return jsonify(before_secret_key[before_token])


@app.route('/verify_token_api/', methods=['GET'])
@token_required
def verify_token_api():
    # token装饰器验证API有效性
    return jsonify({'message': 'hello'})


if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0")
