#!/usr/bin/env python
# -*- coding: utf-8 -*-
from app import api
from flask import jsonify, request
from app.hs_auth.hs_auth_service import *
from huansi_utils.webapi import ApiController
from huansi_utils.ActionFilter import ApiSecurityFilter
from app.common.decorator_token import verify_auth_user1, verify_auth_user2


@api('hs_auth')
class HS_Auth_Provider(ApiController):

    @api('home')
    @verify_auth_user1
    def get_home(self):
        return jsonify('hello')

    @api('app_auth_login')
    def post_access_token(self):
        # app授权登录
        return HS_Auth_UC_Service().app_auth_generate_token(request.get_json())

    @api('app_gen_token')
    def post_gen_refresh_token(self):
        # 生成(续约)token
        return HS_Auth_UC_Service().gen_refresh_token(request.get_json())

    @api('app_verify_token')
    def post_verify_token(self):
        # 校验token时效性
        return HS_Auth_UC_Service().app_verify_token(request.get_json())
