#!/usr/bin/python
# -*- coding: utf-8 -*-
from huansi_utils.server.service_uc import *
from huansi_utils.db.query import *
from huansi_utils.exception.exception import *
from app.common.auth_token import *
from app.common.decorator_token import *


class HS_Auth_UC_Service(HSSingleUCService):
    @property
    def model_class(self):
        return

    def _query_list(self, args):
        return

    def schema_class(self):
        return

    def gen_token(self, grant_type=None, secret_key=None, app_id=None, refresh_token=None, expire_in=None):
        auth_token, time_stamp = generate_auth_token(grant_type, secret_key, app_id, refresh_token, expire_in)
        return auth_token, time_stamp

    def app_auth_generate_token(self, json_data):
        # app授权登录,生成token
        username = json_data.get('username')
        password = json_data.get('password')
        if username and password and username != '' and password != '':
            # 使用%或者format进行拼接,多个变量是倾向于使用这种方法，因为它的可读性更强,chars = '%s%s' % (username, password)
            chars = '{0}{1}'.format(username, password)
            secret_key_random_str = get_random_string(chars)
            print('secret_key_random_str:', secret_key_random_str)
            auth_token, time_stamp = self.gen_token(grant_type='access_token', secret_key=secret_key_random_str[1:],
                                                    expire_in=120)
            return {'auth_token': auth_token, 'time_stamp': time_stamp, 'grant_type': 'access_token'}
        return HSDataError('Invalid username or password')

    def gen_refresh_token(self, json_data):
        # 续约获取新的token
        grant_type = json_data.get('grant_type')
        refresh_token = json_data.get('refresh_token')
        if grant_type == 'refresh_token' and refresh_token:
            auth_token, time_stamp = self.gen_token(grant_type=grant_type, app_id='mes123mes878str',
                                                    refresh_token=refresh_token, expire_in=120)
            return {'auth_token': auth_token, 'time_stamp': time_stamp, 'grant_type': 'refresh_token'}
        return HSDataError('refresh_token params error')

    def app_verify_token(self, json_data):
        grant_type = json_data.get('grant_type')
        auth_token = json_data.get('auth_token')
        # 通过app_id校验取出本系统内的app的secret_key
        app_id = json_data.get('app_id')
        if grant_type == 'refresh_token' and auth_token:
            # mes的唯一标识,app_id
            secret_key = 'mes123mes878str'
            print('app_id', secret_key)
        elif grant_type == 'access_token' and auth_token:
            secret_key = 'ZrSo3MQ015Mb'[1:]
            print('type_secret_key', secret_key)
        else:
            raise HSDataError('Invalid grant_type or auth_token')
        assert verify_auth_token(secret_key, auth_token)
        return {'message': 'Valid auth_token'}

    def sys_login(self):
        return
