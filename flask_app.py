from flask_config import Config
from huansi_utils.app.apploader import AppLoaderBase
from huansi_utils.db.sqlalchemy import db


class FlashAppLoader(AppLoaderBase):

    def init(self, app):
        app.config.from_object(Config)

    def register_blueprint(self, app):
        pass

    def init_db(self, app):
        db.init_app(app)

    def register_webapi(self, app):
        from app import _webApi
        _webApi.init_app(app)


global_app = FlashAppLoader()()
