import os
import time

# 签名key
secretKey = '018f162e804f945ee6b23aebfa863639'

ROOR_BASE_DIR = os.path.abspath(os.path.dirname(__file__))
EXCEL_PATH = os.path.join(ROOR_BASE_DIR, 'static_file/log')


class Config:
    JSON_AS_ASCII = False

    SQLALCHEMY_DATABASE_URI = 'sqlite:////default_test.db'

    # 日志配置
    syslog_tag = "hs_auth"
    LOGCONFIG = {
        'version': 1,
        'disable_existing_loggers': False,
        'filters': {},
        'formatters': {
            "defualt": {
                "format": "[%(asctime)s] %(levelname)s - %(message)s"
            },
            "simple": {
                "format": "[%(asctime)s] %(levelname)s - [FileName:%(filename)s] - [FuncName:%(funcName)s] - [LineNo:%(lineno)s} - %(message)s"
            }
        },
        'handlers': {
            'console': {
                '()': 'logging.StreamHandler',
                'formatter': 'defualt'
            },
            'file': {
                '()': 'logging.handlers.RotatingFileHandler',
                'formatter': 'simple',
                'filename': '{}/log_{}.txt'.format(EXCEL_PATH, time.strftime("%Y-%m-%d")),
                'mode': 'a',
                'maxBytes': 50000,  # 5 MB
                'backupCount': 1,
                "encoding": "utf8"
            },
        },
        'loggers': {
            'myapp': {
                'handlers': ['file'],
                'level': 'DEBUG'
            },
        },
        'root': {
            'handlers': ['console'],
            'level': 'DEBUG'
        }
    }
