from functools import wraps

from flask import jsonify

from huansi_utils.ActionFilter.ApiSecurityFilter import ApiSercurity
from huansi_utils.webapi import ApiController


def ApiSecurityFilter(func):
    '''
    签名验证装饰器
    :param func:
    :return:
    '''

    @wraps(func)
    def inner(*args, **kwargs):
        if getattr(func, '__ignore__', False):
            return func(*args, **kwargs)

        if __debug__:
            print('debug 不作签名校验')
            return func(*args, **kwargs)

        print('签名校验')
        result = ApiSercurity(*args, **kwargs)
        if result:
            if func.__name__ in ['get', 'post', 'delete', 'put']:
                return result, 500
            else:
                return jsonify(result), 500
        return func(*args, **kwargs)

    # 以下代码演示如何添加一个ApiController的类装饰器,以达到所有方法都应用的目的
    if isinstance(func, type) and issubclass(func, ApiController):
        cls = func
        cls.append_method_decorator(ApiSecurityFilter)
        return cls
    return inner


def ingorApiSecurityFilter(func):
    '''
    设置忽略签名的属性用于忽略签名
    :param func:
    :return:
    '''
    setattr(func, '__ignore__', True)
    return func


def Filter(before_func, after_func):
    '''
    函数前后过程装饰器
    :param before_func:
    :param after_func:
    :return:
    '''

    def outer(main_func):
        def wrapper(*args, **kwargs):
            before_result = before_func(*args, **kwargs)
            if before_result:
                return before_result
            main_result = main_func(*args, **kwargs)
            after_result = after_func(*args, **kwargs)
            if main_result:
                return main_result
            if after_result:
                return after_result

        return wrapper

    return outer
