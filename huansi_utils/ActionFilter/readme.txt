API签名校验

example:

from utils.ActionFilter import ApiSecurityFilter

class Test(Resource):
    @ApiSecurityFilter
    def get(self,typeId,openId):
        print(1234)
        return 123

class TestView(Resource):
    @ApiSecurityFilter
    def post(self,openId):
        print(666)
        return '<h1>666</h1>'