from functools import wraps

from flask import request

from huansi_utils.enum.enum import HSSchemaConvertModel


def schema(*view_module):
    '''
    scheam load json装饰器
    :param func:
    :return:
    '''

    def warpper(func):
        @wraps(func)
        def inner(*args, **kwargs):
            print('schema加载json')
            schema_data = json_to_schema(view_module).data
            setattr(request, 'schema_data', schema_data)
            return func(*args, **kwargs)

        return inner

    return warpper


class json_to_schema(object):
    def __init__(self, view_module: tuple):
        # 注册schema到dict中,如果只有一个,直接返回实例,如果多个,返回key为tablename的字典
        # self._register_schema(view_module)
        self.data = self._schema_load(view_module)

    def _schema_load(self, view_module: tuple):
        many = False
        json_data = request.get_json() if request.get_json() else request.form
        if isinstance(json_data, list):
            many = True
        # save_bill json模式和单表 child模式区别
        if len(view_module) > 1 and json_data.get('header'):
            return self._json_to_schema(json_data=json_data, view_module=view_module, type=HSSchemaConvertModel.HdrDtl)
        elif not many and len(view_module) > 1:
            return self._json_to_schema(json_data=json_data, view_module=view_module, type=HSSchemaConvertModel.Child)
        elif many and len(view_module) > 1:
            result = []
            for data in json_data:
                child_data = self._json_to_schema(json_data=data, view_module=view_module,
                                                  type=HSSchemaConvertModel.Child)
                result.append(child_data)
            return result
        else:
            return view_module[0]().new_object(data=json_data, many=many)

    def _json_to_schema(self, json_data, view_module, type):
        if type == HSSchemaConvertModel.Child:
            header_schema_data = view_module[0]().new_object(data=json_data)
            for module in view_module:
                if module == view_module[0]:
                    continue
                # 递归自己拿到child
                child = json_data['child']
                child_schema_data = module().new_object(data=child, many=True)
                header_schema_data._childs = child_schema_data
            return header_schema_data
        elif type == HSSchemaConvertModel.HdrDtl:
            header_schema_data = view_module[0]().new_object(data=json_data['header']['data'])
            for module in view_module:
                child_schema_data = None
                if module == view_module[0]:
                    continue
                for child in json_data['childs']:
                    child_schema_data = module().new_object(data=child['data'], many=True)
                header_schema_data.set_child(child_schema_data)
            return header_schema_data
