import sys
import logging

from flask import Flask
from flask_bootstrap import Bootstrap
from flask_logconfig import LogConfig

PYTHON_VERSION = sys.version_info[0]
logger = logging.getLogger(name='myapp')
# 全局数据校验开关
global_validate_switch = True

# 全局token验证开关
# global_verify_token_switch = True
global_verify_token_switch = False


class AppLoaderBase():

    def __call__(self, *args, **kwargs):
        return self.__app

    @property
    def app(self):
        return self.__app

    def __init__(self):
        self.__app = self.__create_new_app()

    def __create_new_app(self):
        _app = Flask(__name__)

        # 禁用信号发送,减少额外内存
        _app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

        _app.url_map.strict_slashes = True

        self.init(_app)

        LogConfig(_app)
        Bootstrap(_app)

        self.init_db(_app)

        self.register_blueprint(_app)

        self.register_webapi(_app)
        return _app

    def init_db(self, app):
        """
        初始化数据库设置,比如注册orm框架
        :param app: 当前创建的Flask app
        :return:
        """
        pass

    def register_blueprint(self, app):
        """
        注册blueprint
        :param app:当前创建的Flask app
        :return:
        """
        pass

    def register_webapi(self, app):
        """
        注册webapi
        :param app:当前创建的Flask app
        :return:
        """
        pass

    def init(self, app):
        """
        要在初始化时做的其他事情,由子类控制
        :param app:当前创建的Flask app
        :return:
        """
        pass
