# global_bill_types={54:IM_arrive_service,90:IM_store_service}
global_bill_types = {}
test_cases = []


def register_bill_type(bill_type, cls):
    global_bill_types[bill_type] = cls


def register_test_services(cls):
    test_cases.append(cls)

def HSRegisterBillType(bill_type=None):
    '''
    注册单据类型的装饰器
    :param bill_type:
    :return:
    '''
    def decorator(func):
        cls = func.__new__(func) if func else None
        bill_type_id = bill_type if bill_type else cls.bill_type
        global_bill_types[bill_type_id] = func
        return func

    return decorator
