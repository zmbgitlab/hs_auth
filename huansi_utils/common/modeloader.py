import os


def dynamic_import(import_prefix, root_dir, file_suffix=".py"):
    """
    动态导入模块
    :param import_prefix:导入模块的前缀
    :param root_dir:扫描的根目录
    :param file_suffix:文件后缀
    :return:
    """

    def suffix_is_match(file):
        if isinstance(file_suffix, str):
            return file.lower().endswith(file_suffix.lower())
        return any(filter(lambda x: file.lower().endswith(x.lower()), file_suffix))

    for root, dirs, files in os.walk(root_dir):
        # logger.debug('路径{}'.format(root_dir))
        for file in files:
            # logger.debug('{},{},{}'.format(file.startswith("__"), 111, suffix_is_match(file)))
            if file.startswith("__") or not suffix_is_match(file):
                continue
            module_name = "{}.{}".format(import_prefix, os.path.splitext(file)[0])
            # logger.debug('导入前{}'.format(module_name))
            __import__(module_name)
            # logger.debug('导入后{}'.format(module_name))
        # 递归调用
        for _dir in dirs:
            # .开头的文件夹不处理和utils的文件夹不作处理
            if _dir == "__pycache__" or _dir.startswith(".") or _dir == 'huansi_utils':
                continue
            dynamic_import(import_prefix + '.' + _dir, os.path.join(root, _dir), file_suffix)

        # 由于自己递归了,所以不再往下递归
        break
