import time


# 拆分字符串
# 按指定字符分解字符串
# </summary>
# str:原字符串
# p1:排除范围的前置字符
# p2:排除范围的后置字符
# splitor:分隔符
# bIncludeSplitor:是否包含分隔符
# sNotIn:排除范围的前后置字符数组
# "AA(a,b),BB(c,d)".m_Split(",","(",")")==>AA(a,b)|BB(c,d)
# <returns>如不在{}之内的逗号分隔</returns>
# @staticmethod
def split_string(str, splitor=',', p1=None, p2=None, bIncludeSplitor=False, bIncludeEmpty=False, sNotIn=[]):
    return str.strip(',').split(splitor)


def str_to_int(str, default=None):
    if isinstance(str, int):
        return str
    elif not str:
        return default
    try:
        if isinstance(str, int) is False:
            return str
        value = int(str)
    except Exception as e:
        value = default
    return value


def get_bill_date():
    '''
    获取当前时间的%Y-%m-%d格式
    :return:
    '''
    return time.strftime('%Y-%m-%d', time.localtime(time.time()))


def before_string(str: str, start: str = ''):
    '''
    从源字符串获取指定字符串之前的字符串

    :param str: 源字符串
    :param start: 指定字符串（开始字符串）
    :param i_start: 从i_start 下标开始查找开始字符串的位置
    :return:
    '''
    res = str
    if str and start and len(start) > 0:
        str_find = str.find(start)
        if str_find != -1:
            res = str[:str_find]
    return res


def after_string(str, start='', i_start=0):
    '''

    从源字符串获取指定字符串之后的字符串

    :param str: 源字符串
    :param start: 指定字符串（开始字符串）
    :param i_start: 从i_start 下标开始查找开始字符串的位置
    :return:
    '''
    res = str
    if str and start and len(start) > 0:
        str_find = str.find(start, i_start)
        if str_find != -1:
            res = str[str_find + len(start):]
    return res


def between_string(str, start='', end='', i_start=0):
    """
    获取源字符串的中间字符串

    :param str: 源字符串
    :param start: 开始字符串
    :param end: 结束字符串
    :param i_start: 从i_start 下标开始查找开始字符串的位置
    :param i_end: 从开始字符串位置+i_end 开始查找结束字符串的位置
    :return: 目标字符串
    """

    if not str:
        str = ""

    result = str

    if (not start or start == "") and (not end or end == ""):
        pass

    elif not start or start == "":
        result = before_string(str, end)

    elif not end or end == "":
        result = after_string(str, start, i_start)

    else:
        iStart = str.find(start, i_start)
        i_start = iStart + len(iStart)
        iEnd = str.find(start, i_start)
        if iStart < 0 or iEnd < 0:
            result = ''
        else:
            result = str[iStart:iEnd]
    return result
