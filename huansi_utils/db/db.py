import uuid

from flask import g

from flask_app import db
# from .huansi_util_common import debug
from huansi_utils.common.IdWorker import *
from huansi_utils.common.logger import InstanceLogger
from huansi_utils.db.dbsession import HSDBSession

'''id生成器规则'''
idWorker = IdWorker(9, 9)

'''自定义logger'''
hs_logger = InstanceLogger(False, 'sqlalchemy.engine.base.Engine')

'''session'''
hs_session = db.create_scoped_session(options={'autocommit': False, 'autoflush': False})


def new_session(begin=False, total_rollback_count=1):
    session = HSDBSession(db_session=hs_session, total_rollback_count=total_rollback_count)
    if begin:
        session.begin_trans()
    # 注入自定义logger
    session.db_session.bind.logger = hs_logger
    return session


def get_session():
    return db.create_scoped_session(options={
        'autocommit': False,
        'autoflush': False
    })


def new_id():
    return idWorker.get_id()


def new_id_by_sql():
    sql = 'SELECT id=dbo.fnpbNextId()'
    return hs_session.execute(sql).first().id


def new_guid():
    return uuid.uuid1()


def new_bill_no(formula_name=None, formula_id=None):
    return new_bill_no_by_sql(formula_name, formula_id)


def db_session():
    return g.hs_session


def new_bill_no_by_sql(formula_name=None, formula_id=None):
    if not formula_id:
        formula_id = 0
    if not formula_name:
        formula_name = ''
    sql = '''DECLARE @sBillNo NVARCHAR(20)=0x
    EXEC dbo.sppbNewBillNo @iFormulaId={0},@sFormulaName='{1}',@sNewbillNo=@sBillNo OUTPUT
    SELECT sBillNo=@sBillNo
    '''.format(formula_id, formula_name)
    # print('formulaid=', formula_id, 'formula_name=', formula_name, 'sql=', sql)
    try:
        data = db_session().retrive_sql(sql)
        data = data.sBillNo if data else ''
        return str(data)
    except Exception as e:
        print('生成单据号出错,sql=', sql)
        print(e)
        raise


def get_model_column_names(model):
    '''
    返回model的所有列名
    :param model: 实体对象
    :return: 列名的元组，如('id','bill_no')
    '''
    if hasattr(model, '_fields'):
        return model._fields
    else:
        return tuple([v.name for v in model.__table__.columns])


def get_model_columns(model):
    '''
    返回model的列信息(列名+列类型)
    :param model: 实体对象
    :return: 如[{'id': <class 'str'>}, {'bill_no': <class 'str'>}]
    或[{'id': BigInteger()}, {'bill_date': Date()}, {'bill_status': Integer()}, {'creator_name': NVARCHAR(length=20)}]
    '''
    if hasattr(model, '_fields'):
        return [{v: type(v)} for v in model._fields]
    else:
        return [{v.name: v.type} for v in model.__table__.columns]


def get_model_all_Columns(model):
    '''
    返回model的所有列信息，仅按类查询时才有效
    :param model: 实体对象
    :return: 所有列对象列表[Column(),Column()]
    '''
    return model.__table__.columns if hasattr(model, '__table__') else None


def get_model_class_columns(cls, columns):
    # return IM_arrive.provider_name
    return tuple([getattr(cls, column) for column in columns])


def model_load_json_data(model, json_data, only_set_null_column=True, ignore_columns=None):
    '''
    从json对象中装载数据到实体中
    :param model: 实体对象
    :param json_data: json对象
    :param only_set_null_column: 是否仅设置空值列
    :param ignore_columns: 忽略字段列表,如'id,create_time'
    :return: model
    '''
    if not json_data:
        return model
    columns = get_model_column_names(model)
    if ignore_columns:
        ignore_columns = ignore_columns.strip(',').split(',')
        columns = set(columns).difference(ignore_columns)
    columns = set(json_data.keys()).intersection(columns)
    for column in columns:
        if not hasattr(model, column):
            continue
        old_value = getattr(model, column)
        new_value = json_data.get(column)
        if not only_set_null_column or old_value is None:
            setattr(model, column, new_value)
    return model
