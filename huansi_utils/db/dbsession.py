# from huansi_utils.huansi_util_common import debug

from flask_sqlalchemy import BaseQuery
from sqlalchemy.engine import ResultProxy

from huansi_utils.app.apploader import logger
from sqlalchemy import text
from huansi_utils.exception.exception import HSSQLError


class HSDBSession(object):
    def __init__(self, db_session, total_rollback_count=None):
        super().__init__()
        self.db_session = db_session
        self.trans_count = 0
        # 验证可Rollback的总次数
        self._total_rollback_count = total_rollback_count if total_rollback_count else 0
        self._rollback_count = 0

    def flush(self):
        self.db_session.flush()

    def begin_trans(self):
        if self._total_rollback_count > 0:
            if self._rollback_count > self._total_rollback_count:
                raise RuntimeError('事务已回滚，不能再启动事务')
        # SQLAlchemy的事务是自动启动的，不需要手动Begin
        if self.trans_count <= 0:
            self.trans_count = 0
        self.trans_count += 1
        # print('begin_tran:', self.trans_count)

    def commit_trans(self, close=False):
        if self._total_rollback_count > 0:
            if self._rollback_count > self._total_rollback_count:
                raise RuntimeError('事务已回滚，不能再启动事务')
        # print('before.commit_trans:', self.trans_count)
        self.db_session.flush()
        self.trans_count -= 1
        if self.trans_count == 0:
            self.db_session.commit()
        if close:
            self.close()

    def rollback_trans(self, close=False):
        # print('before.rollback_trans:', self.trans_count)
        self.trans_count = 0
        self.db_session.rollback()
        self._rollback_count += 1
        if close:
            self.close()

    def close(self):
        # print('close_session:', self.trans_count)
        in_trans = self.trans_count > 0
        if in_trans:
            self.rollback_trans()
        self.db_session.close()
        if in_trans:
            raise RuntimeError('当前还在事务中，已自动回滚并关闭连接')

    def execute(self, sql: str, **kwargs) -> ResultProxy:
        '''
        执行sql过程
        :param sql: str
        :param kwargs:
        :return:
        '''
        result = self.db_session.execute(text(sql), kwargs)
        self.log_sql(result)
        return result

    def get_sql_from_query(self, query: BaseQuery) -> str:
        '''
        返回query中的sql语句
        :param query:
        :return: str
        '''
        if not query:
            return ''
        sql_state = query.statement.compile()
        sql = str(sql_state.statement)
        for key, value in sql_state.params.items():
            sql = sql.replace(':{}'.format(key), "'{}'".format(str(value)))
        return sql

    def log_sql(self, query: (BaseQuery, ResultProxy)) -> None:
        '''
        记录sql
        :param query:
        :return:
        '''
        return
        # if g.debug_sql:
        #     sql = ''
        #     if isinstance(query, ResultProxy):
        #         sql = str(query.context.compiled)
        #         params = query.context.parameters[0]
        #         for key, value in params.items():
        #             sql = sql.replace('%({})s'.format(key), str(value))
        #     elif isinstance(query, BaseQuery):
        #         sql = self.get_sql_from_query(query)
        #     g.sql.append(sql)

    def exec_sql(self, sql, **kwargs):
        '''
        执行SQL
        :param sql: sql语句
        :param kwargs: 参数
        :return: SQL执行影响行数
        '''
        if not sql:
            return -1
        try:
            # self.begin_trans()
            sqlEx = '{}\n{}'.format(sql, 'select _rowCount = @@RowCount')
            data = self.execute(sqlEx, **kwargs).first()
            if getattr(data, '_rowCount', None) is not None:
                rowcount = data._rowCount
            else:
                logger.error('EXEC过程中出现select语句并且报错了，{}'.format(sql))
                raise HSSQLError('EXEC过程中出现select语句并且报错了，{}'.format(sql))
            # self.commit_trans()
            return rowcount
        except Exception as e:
            logger.error('db_session.exec_sql出错，sql=', sql)
            logger.error(e)
            raise HSSQLError(e, 'db_session.exec_sql出错[SQL:{}]'.format(sql))

    # 执行SQL
    def retrive_sql(self, sql, **kwargs):
        '''
        执行SQL，并返回单行数据
        :param sql: sql语句
        :param kwargs: 参数
        :return: 执行返回的单行数据
        '''
        if not sql:
            return None
        try:
            # self.begin_trans()
            data = self.execute(sql, **kwargs).first()
            # self.commit_trans()
            return data
        except Exception as e:
            logger.error('db_session.retrive_sql出错，sql=', sql)
            logger.error(e)
            raise HSSQLError(e, 'db_session.retrive_sql出错[SQL:{}]'.format(sql))

    def query_sql(self, sql, **kwargs):
        '''
        执行SQL，并返回多行数据
        :param sql: sql语句
        :param kwargs: 参数
        :return: 执行返回的多行数据
        '''
        if not sql:
            return None
        try:
            # self.begin_trans()
            data = self.execute(sql, **kwargs).fetchall()
            # self.commit_trans()
            return data
        except Exception as e:
            # self.rollback_trans()
            logger.error('db_session.query_sql出错，sql=', sql)
            logger.error(e)
            raise HSSQLError(e, 'db_session.query_sql出错[SQL:{}]'.format(sql))

    def query_sql_to_many_set(self, sql, **kwargs):
        try:
            query_data = self.execute(sql, **kwargs)
            result = {}
            count = 1
            while 1:
                name = 'set{}'.format(count)
                data_set = query_data.cursor.fetchall()
                fields = [c[0] for c in query_data.cursor.description]

                _data = []
                for date_set_item in data_set:
                    _data.append(dict(map(lambda x, y: [x, y], fields, date_set_item)))
                result[name] = _data

                if query_data.cursor.nextset() is None:
                    break

                count += 1
            return result
        except Exception as e:
            logger.error('db_session.query_sql出错，sql=', sql)
            logger.error(e)
            raise HSSQLError(e)
