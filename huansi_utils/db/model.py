import datetime

from huansi_utils.common.string import split_string
from huansi_utils.db.db import get_model_column_names, new_id,new_bill_no,new_guid
from huansi_utils.db.sqlalchemy import db


class HSBaseModel(object):
    __tablename__ = ''

    def retrive(self, db_session, exclude_columns=None, include_columns=None):
        id = self.id
        if not id:
            raise RuntimeError('未指定主键，不能retrive')
        columns = get_model_column_names(model=self)
        if exclude_columns:
            exclude_columns = split_string(exclude_columns)
            columns = ','.join(set(columns).difference(exclude_columns))
        elif include_columns:
            include_columns = split_string(include_columns)
            columns = ','.join(set(columns).intersection(include_columns))
        else:
            columns = '*'
        sql = 'SELECT TOP 1 {0} FROM dbo.{1} A(NOLOCK) WHERE A.id={2}'.format(columns, self.__tablename__, id)
        obj = db_session.retrive_sql(sql)
        # print('retrive.data:', data)
        return self.load_object(obj)

    def load_json(self, json_data, only_set_null_column=True, ignore_columns=None):
        '''
        从json对象中装载数据到当前实体中
        :param self: 当前实体对象
        :param json_data: json对象
        :param only_set_null_column: 是否仅设置空值列
        :param ignore_columns: 忽略字段列表,如'id,create_time'
        :return: self
        '''
        if not json_data:
            return self
        columns = get_model_column_names(self)
        if ignore_columns:
            ignore_columns = ignore_columns.strip(',').split(',')
            columns = set(columns).difference(ignore_columns)
        columns = set(json_data.keys()).intersection(columns)
        for column in columns:
            if not hasattr(self, column):
                continue
            old_value = getattr(self, column)
            new_value = json_data.get(column)
            # print('old_value:', old_value, column)
            # print('new_value:', new_value, column)
            if not only_set_null_column or old_value is None:
                setattr(self, column, new_value)
        return self

    def load_object(self, obj, only_set_null_column=True, ignore_columns=None):
        '''
        从obj对象中装载数据到当前实体中
        :param obj: 数据对象
        :param only_set_null_column: 是否仅设置空值列
        :param ignore_columns: 忽略字段列表,如'id,create_time'
        :return: self
        '''
        if not obj:
            return self
        columns = get_model_column_names(model=self)
        if ignore_columns:
            ignore_columns = ignore_columns.strip(',').split(',')
            columns = set(columns).difference(ignore_columns)
        for column in columns:
            if column == 'iHdrId':
                column = 'bill_id'
            if not hasattr(self, column):
                continue
            old_value = getattr(self, column)

            new_value = getattr(obj, column) if hasattr(obj, column) else None

            if not new_value is None:
                if not only_set_null_column or old_value is None:
                    setattr(self, column, new_value)
        return self


class HSBaseTableModel(HSBaseModel):
    '''
    基础Model
    '''
    id = db.Column(
        db.BigInteger,
        primary_key=True,
        nullable=False,
        unique=True,
        default=lambda: new_id())


class HSBaseArchiveModel(HSBaseTableModel):
    '''
    基础主档Model
    '''
    creator_name = db.Column(db.NVARCHAR(20), nullable=True, default='')
    create_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)
    modify_name = db.Column(db.NVARCHAR(20), nullable=True)
    modify_time = db.Column(
        db.DateTime, nullable=True, default=datetime.datetime.now, onupdate=datetime.datetime.now)
    usable = db.Column(db.Boolean, nullable=False, default=1)
    # time_stamp = db.Column(db.TIMESTAMP, nullable=True)


class HSBaseBillHdrModel(HSBaseTableModel):
    '''
    基础单据头Model
    '''
    bill_no = db.Column(db.NVARCHAR(20), nullable=True, unique=True)
    bill_date = db.Column(db.Date, nullable=True)
    bill_status = db.Column(db.Integer, nullable=False, default=0)
    creator_name = db.Column(db.NVARCHAR(20), nullable=False, default='')
    create_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)
    # company_id = db.Column(db.INT, nullable=False, default=1)
    # auditor_name = db.Column(db.NVARCHAR(20), nullable=True)
    # audit_time = db.Column(db.DateTime, nullable=True)
    # time_stamp = db.Column(db.TIMESTAMP, nullable=True)
    # dtl_time_stamp = db.Column(db.BigInteger, nullable=False, default=0)
    is_system = db.Column(db.Boolean, nullable=False, default=0)


class HSBaseBillDtlModel(HSBaseTableModel):
    '''
    基础单据明细Model
    '''
    bill_id = db.Column(db.BigInteger, nullable=False)
    # row_id = db.Column(db.Integer, nullable=True, default=0)
    # time_stamp = db.Column(db.TIMESTAMP, nullable=True)


class HSBillDtlModel(HSBaseBillDtlModel):
    '''
    单据明细Model
    '''
    ref_bill_dtl_id = db.Column(db.BigInteger, nullable=False, default=0)
    ref_bill_type = db.Column(db.Integer, nullable=False, default=0)


# <editor-fold desc="(旧版本表结构iIden为主键)">
class HSModel_BaseTable(HSBaseModel):
    '''
    基础表Model(旧版本表结构iIden为主键)
    '''
    id = db.Column(
        db.BigInteger,
        name="iIden",
        primary_key=True,
        nullable=False,
        unique=True,
        default=lambda: new_id())


class HSModel_BaseArchive(HSModel_BaseTable):
    '''
    基础主档Model(旧版本表结构iIden为主键)
    '''
    sCreator = db.Column(db.NVARCHAR(20), nullable=True)
    tCreateTime = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)
    sUpdateMan = db.Column(db.NVARCHAR(20), nullable=True)
    tUpdateTime = db.Column(db.DateTime, nullable=True, default=datetime.datetime.now, onupdate=datetime.datetime.now)
    bUsable = db.Column(db.Boolean, nullable=False, default=1)
    # tTimestamp = db.Column(db.TIMESTAMP, nullable=True)


class HSModel_BaseBillHdr(HSModel_BaseTable):
    '''
    基础头表Model(旧版本表结构iIden为主键)
    '''
    iCompanyId = db.Column(db.BigInteger, nullable=False, default=1)
    sBillNo = db.Column(db.NVARCHAR(20), nullable=True, unique=True)
    dBillDate = db.Column(db.Date, nullable=True)
    iBillStatus = db.Column(db.Integer, nullable=False, default=0)
    sCreator = db.Column(db.NVARCHAR(20), nullable=True)
    tCreateTime = db.Column(db.DateTime, nullable=False, default=datetime.datetime.now)
    # tTimeStamp = db.Column(db.TIMESTAMP, nullable=True)
    # iDtlTimeStamp = db.Column(db.BigInteger, nullable=False, default=0)

    # bSystem = db.Column(db.Bit, nullable=False, default=0)
    # iSystemBillStyle = db.Column(db.Integer, nullable=False, default=0)
    # iCreator = db.Column(db.BigInteger, nullable=False, default=0)
    # iDepartmentId = db.Column(db.BigInteger, nullable=False, default=0)


class HSModel_BaseBillDtl(HSModel_BaseTable):
    '''
    基础明细表(旧版本表结构iIden为主键)
    '''
    bill_id = db.Column(db.BigInteger, nullable=False, name='iHdrId')
    # iRowNo = db.Column(db.Integer, nullable=True, default=0)
    # tTimeStamp = db.Column(db.TIMESTAMP, nullable=True)


# [0]
class HSModel_BillDtl(db.Model, HSModel_BaseBillHdr):
    '''
    单据明细Model(旧版本表结构iIden为主键)
    '''
    iRefBillType = db.Column(db.Integer, nullable=False, default=0)
    iRefDtlId = db.Column(db.BigInteger, nullable=False, default=0)

# </editor-fold>
