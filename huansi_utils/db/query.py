from sqlalchemy import or_, and_

from huansi_utils.enum.enum import HSSearchMode
from huansi_utils.exception.exception import HSArgumentError, HSException


class HSConditionCalc(object):
    def __init__(self, args):
        if isinstance(args, HSArgCalc):
            self.argCalc = args
        else:
            self.argCalc = HSArgCalc(args=args)
        self._condition = None
        self.sqlArgCalc = HSSqlArgCalc(args)

    def merge_condition(self, c):
        self._condition = self.and_condition(self._condition, c)
        return self

    def and_condition(self, c1, c2):
        if c1 is None:
            return c2
        elif c2 is None:
            return c1
        else:
            return and_(c1, c2)

    def or_condition(self, c1, c2):
        if c1 is None:
            return c2
        elif c2 is None:
            return c1
        else:
            return or_(c1, c2)

    def quick_query(self, *columns, search_mode=HSSearchMode.Combo):
        '''
        计算速查条件
        '''
        quick_query_field = 'quick_query'
        quick_query_value = self.argCalc.args.get(quick_query_field)
        if not quick_query_value:
            return self
        result = None
        for column in columns:
            if search_mode == HSSearchMode.Sharp:
                c = self.argCalc.equal(column, value=quick_query_value)
            elif search_mode == HSSearchMode.Left:
                c = self.argCalc.like(column, value=quick_query_value, match='{}%')
            elif search_mode == HSSearchMode.Like:
                c = self.argCalc.like(column, value=quick_query_value)
            elif search_mode == HSSearchMode.Combo:
                c = None
                for v1 in quick_query_value.split(','):
                    c1 = None
                    for v2 in v1.split(' '):
                        c2 = self.argCalc.like(column, value=v2)
                        c1 = self.and_condition(c1, c2)
                    c = self.or_condition(c, c1)
            else:
                c = None
            result = self.or_condition(result, c)
        return self.merge_condition(result)

    @property
    def condition(self):
        if self._condition is None:
            return and_('1=1')
        else:
            return self._condition

    def where(self, expression):
        '''
        添加筛选条件
        :param expression: 表达式
        '''
        return self.merge_condition(expression)

    def where_equal(self, *columns, column_name=None, value=None):
        c = self.argCalc.equal(*columns, column_name=column_name, value=value)
        return self.merge_condition(c)

    def where_not_equal(self, *columns, column_name=None, value=None):
        c = self.argCalc.not_equal(*columns, column_name=column_name, value=value)
        return self.merge_condition(c)

    def where_like(self, *columns, column_name=None, value=None, match='%{}%', search_mode=HSSearchMode.Combo):
        c = self.argCalc.like(*columns, column_name=column_name, value=value, match=match, search_mode=search_mode,
                              like=False)
        return self.merge_condition(c)

    def where_in(self, *columns, column_name=None, value=None):
        c = self.argCalc.in_(*columns, column_name=column_name, value=value)
        return self.merge_condition(c)

    def where_not_in(self, *columns, column_name=None, value=None):
        c = self.argCalc.not_in(*columns, column_name=column_name, value=value)
        return self.merge_condition(c)

    def where_between(self, column, before_column_name=None, after_column_name=None):
        c = self.argCalc.between(column, before_column_name=before_column_name, after_column_name=after_column_name)
        return self.merge_condition(c)

    def where_greater_than(self, column, column_name=None, value=None, include_equal=False):
        c = self.argCalc.greater_than(column, column_name=column_name, value=value, include_equal=include_equal)
        return self.merge_condition(c)

    def where_less_than(self, column, column_name=None, value=None, include_equal=False):
        c = self.argCalc.less_than(column, column_name=column_name, value=value, include_equal=include_equal)
        return self.merge_condition(c)


class HSArgCalc(object):
    def __init__(self, args):
        self.args = args
        self.sqlArgCalc = HSSqlArgCalc(args)

    def validate_not_null(self, fields):
        column_list = ''
        for column in fields.split(','):
            if self.args.get(column) is None or self.args.get(column) == '':
                column_list += '%s 的值为空，' % column
        if column_list:
            raise HSArgumentError(column_list)

    def validate_value(self, column, value):
        '''
        验证args里面的参数值是否正确
        (目前只做了int bigInt的判断)
        :param column: 字段名 (Model.id)
        :param value:  值
        :return:
        '''
        if isinstance(value, list):
            return
        if str(column.expression.type) in ['BIGINT', 'INTEGER'] and value is not None:
            try:
                int(value)
            except ValueError:
                error_message = '{}不是int类型或bigInt类型'.format(column.key)
                raise HSException(error_message)

    def merge_condition(self, c1, c2, like=False):
        if c1 is None:
            return c2
        elif c2 is None:
            return c1
        elif like is True:
            return or_(c1, c2)
        else:
            return and_(c1, c2)

    def equal(self, *columns, column_name=None, value=None):
        filter = None
        for column in columns:
            scolumn_name = column_name if column_name else column.key
            v = value if value else self.args.get(scolumn_name)
            self.validate_value(column=column, value=v)
            if v:
                c = (column == v)
                filter = self.merge_condition(c1=filter, c2=c)
        return filter if filter is not None else '1=1'

    def not_equal(self, *columns, column_name=None, value=None):
        filter = None
        for column in columns:
            scolumn_name = column_name if column_name else column.key
            v = value if value else self.args.get(scolumn_name)
            self.validate_value(column=column, value=v)
            if v:
                c = (column != v)
                filter = self.merge_condition(c1=filter, c2=c)
        return filter if filter is not None else '1=1'

    def like(self, *columns, column_name=None, value=None, match='%{}%', search_mode=HSSearchMode.Combo, like=True):
        filter = None
        for column in columns:
            scolumn_name = column_name if column_name else column.key
            v = value if value else self.args.get(scolumn_name)
            self.validate_value(column=column, value=v)
            if v:
                c = column.like(match.format(v))
                filter = self.merge_condition(c1=filter, c2=c, like=like)
            # if search_mode == HSSearchMode.Combo:
            #     c = None
            #     for v1 in v.split(','):
            #         c1 = None
            #         for v2 in v1.split(' '):
            #             c2 = column.like(match.format(v2))
            #             c1 = self.merge_condition(c1, c2)
            #         c = self.merge_condition(c, c1, like=True)
            #     return c
            # elif v:
            #     c = column.like(match.format(v))
            #     filter = self.merge_condition(c1=filter, c2=c, like=like)
        return filter if filter is not None else '1=1'

    def in_(self, *columns, column_name=None, value=None):
        filter = None
        for column in columns:
            scolumn_name = column_name if column_name else column.key
            v = value if value else self.args.get(scolumn_name)
            if v:
                if isinstance(v, str):
                    v = v.split(',')
                c = column.in_(v)
                filter = self.merge_condition(c1=filter, c2=c)
        return filter if filter is not None else '1=1'

    def not_in(self, *columns, column_name=None, value=None):
        filter = None
        for column in columns:
            scolumn_name = column_name if column_name else column.key
            v = value if value else self.args.get(scolumn_name)
            self.validate_value(column=column, value=v)
            if v:
                c = column.notin_(v)
                filter = self.merge_condition(c1=filter, c2=c)
        return filter if filter is not None else '1=1'

    def between(self, column, before_column_name=None, after_column_name=None):
        filter = None
        before_value = self.args.get(before_column_name)
        if before_value:
            c = self.greater_than(column, column_name=before_column_name, value=before_value, include_equal=True)
            filter = self.merge_condition(c1=filter, c2=c)
        after_value = self.args.get(after_column_name)
        if after_value:
            c = self.less_than(column, column_name=after_column_name, value=after_value, include_equal=True)
            filter = self.merge_condition(c1=filter, c2=c)
        return filter if filter is not None else '1=1'

    def greater_than(self, column, column_name=None, value=None, include_equal=False):
        filter = None
        scolumn_name = column_name if column_name else column.key
        v = value if value else self.args.get(scolumn_name)
        self.validate_value(column=column, value=v)
        if v:
            c = (column >= v) if include_equal else (column > v)
            filter = self.merge_condition(c1=filter, c2=c)
        return filter if filter is not None else '1=1'

    def less_than(self, column, column_name=None, value=None, include_equal=False):
        filter = None
        scolumn_name = column_name if column_name else column.key
        v = value if value else self.args.get(scolumn_name)
        self.validate_value(column=column, value=v)
        if v:
            c = (column <= v) if include_equal else (column < v)
            filter = self.merge_condition(c1=filter, c2=c)
        return filter if filter is not None else '1=1'


class HSSqlArgCalc(object):
    '''
    SQL原生查询语句生成
    args = {'id':123,'size_name':'ABC','quantity':50,'id_list':['9','8','7','6','5',4]
            ,'id_string':'1,2,3,4,5','start':22,'end':33,'quick_query':'123'}

    print(HSSqlArgCalc(args)
          .quick_query('A.id','B.size_name')
          .where_like('C.size_name')
          .where('A.quantity>100')
          .where_between('A.number','strat','end')
          .where_in('B.id_list')
          .where_not_in('C.id_string')
          .condition)
    :param args (dict) 字典参数
    :return(string) C.id_string not in '(1,2,3,4,5)'
            AND B.id_list in '(9,8,7,6,5,4)'
            AND A.number <= '33'
            AND A.quantity>100
            AND C.size_name like '%ABC%'
            AND (((A.id like '%123%')) OR (B.size_name like '%123%'))
    '''

    def __init__(self, args):
        self.args = args
        self._condition = None

    @property
    def condition(self):
        return self._condition if self._condition else '1=1'

    def validate_value(self, column, value):
        '''
        验证args里面的参数值是否正确
        (目前只做了int bigInt的判断)
        :param column: 字段名 (A.id or B.iIden)
        :param value:  值
        :return:
        '''
        if not value:
            return
        if '.' in column:
            _column = column.split('.')[1]
        else:
            _column = column
        if _column[-2:] == 'id' or _column[:1] == 'i':
            try:
                int(value)
            except ValueError:
                error_message = '{}不是int类型或bigInt类型'.format(_column)
                raise HSException(error_message)

    def _merge_condition(self, c):
        self._condition = self._and_condition(self._condition, c)

    def _get_value(self, column):
        if '.' in column:
            column = column.split('.')[1]
        return self.args.get(column)

    def _and_condition(self, c1, c2):
        if c1 is None:
            return c2
        elif c2 is None:
            return c1
        else:
            return '{} AND {}'.format(c2, c1)

    def _or_condition(self, c1, c2):
        if c1 is None:
            return c2
        elif c2 is None:
            return c1
        else:
            return '{} OR {}'.format(c1, c2)

    def quick_query(self, *columns, search_mode=HSSearchMode.Combo):
        '''
        计算速查条件
        '''
        quick_query_field = 'quick_query'
        quick_query_value = self._get_value(quick_query_field)
        if not quick_query_value:
            return self
        c = None
        for column in columns:
            if search_mode == HSSearchMode.Sharp:
                c = self.where_equal(column, value=quick_query_value)
            elif search_mode == HSSearchMode.Left:
                c = self.where_like(column, value=quick_query_value, match='{}%')
            elif search_mode == HSSearchMode.Like:
                c = self.where_like(column, value=quick_query_value)
            elif search_mode == HSSearchMode.Combo:
                for v1 in quick_query_value.split(','):
                    c1 = None
                    # 单个参数被split也会生成list，所以要对长度进行判断
                    space_param = v1.split(' ')
                    for v2 in space_param:
                        c2 = "{} like '%{}%'".format(column, v2)
                        c1 = self._and_condition(c1, c2)
                    if c1 is not None:
                        c1 = '({})'.format(c1)
                    c = self._or_condition(c1, c)
                if c is not None:
                    c = '({})'.format(c)
                    self._merge_condition(c)
            else:
                c = None
        self._merge_condition(c)
        return self

    def where(self, expression):
        '''
        where条件
        where("(A.id>10 and B.id<10) or C.size_name != 'L'")
        :param expression: sql表达式 "(A.id>10 and B.id<10) or C.size_name != 'L'"
        :return:
        '''
        self._merge_condition(expression)
        return self

    def where_equal(self, *columns, column_name=None, value=None):
        '''
        sql的equal条件
        :param clomuns: 字段 'A.id','B.id'
        :param value: 默认值
        :return:
        '''
        for column in columns:
            column_name_fact = column_name if column_name else column
            column_value = value if value else self._get_value(column_name_fact)
            self.validate_value(column=column, value=column_value)
            if column_value:
                _condition = "{}='{}'".format(column, column_value)
                self._merge_condition(_condition)
        return self

    def where_like(self, *columns, column_name=None, value=None, match='%{}%', search_mode=HSSearchMode.Combo):
        for column in columns:
            c = None
            column_name_fact = column_name if column_name else column
            column_value = value if value else self._get_value(column_name_fact)
            if column_value:
                self.validate_value(column=column, value=column_value)
                if search_mode == HSSearchMode.Combo:
                    for v1 in column_value.split(','):
                        c1 = None
                        # 单个参数被split也会生成list，所以要对长度进行判断
                        space_param = v1.split(' ')
                        for v2 in space_param:
                            c2 = "{} like '%{}%'".format(column, v2)
                            c1 = self._and_condition(c1, c2)
                        if c1 is not None:
                            c1 = '({})'.format(c1)
                        c = self._or_condition(c1, c)
                    if c is not None:
                        c = '({})'.format(c)
                        self._merge_condition(c)
                elif search_mode == HSSearchMode.Like:
                    _condition = "{} like '{}'".format(column, match.format(column_value))
                    self._merge_condition(_condition)
        return self

    def where_in(self, *columns, column_name=None, value=None):
        for column in columns:
            column_name_fact = column_name if column_name else column
            column_value = value if value else self._get_value(column_name_fact)
            self.validate_value(column=column, value=column_value)
            if isinstance(column_value, (list, tuple)):
                column_value = str(list(column_value)).strip('[]')
            if column_value:
                if ',' in column_value:
                    column_value = (''' ',' ''').join([str(item) for item in column_value.split(',')])
                _condition = "{} in ('{}')".format(column, column_value)
                self._merge_condition(_condition)
        return self

    def where_not_in(self, *columns, column_name=None, value=None):
        for column in columns:
            column_name_fact = column_name if column_name else column
            column_value = value if value else self._get_value(column_name_fact)
            self.validate_value(column=column, value=column_value)
            if isinstance(column_value, (list, tuple)):
                column_value = [str(item) for item in column_value]
                column_value = ','.join(column_value)
            if column_value:
                _condition = "{} not in '({})'".format(column, column_value)
                self._merge_condition(_condition)
        return self

    def where_between(self, column, before_column_name=None, after_column_name=None):
        before_value = self._get_value(before_column_name)
        if before_value:
            self.where_greater_than(column, value=before_value, include_equal=True)
        after_value = self._get_value(after_column_name)
        if after_value:
            self.where_less_than(column, value=after_value, include_equal=True)
        return self

    def where_greater_than(self, column, column_name=None, value=None, include_equal=False):
        if not column_name:
            column_name = column
        column_value = value if value else self._get_value(column_name)
        self.validate_value(column=column_name, value=column_value)
        if column_value:
            _condition = "{} >= '{}'".format(column, column_value) if include_equal else "{} > {}".format(column,
                                                                                                          column_value)
            self._merge_condition(_condition)
        return self

    def where_less_than(self, column, column_name=None, value=None, include_equal=False):
        column_name_fact = column_name if column_name else column
        column_value = value if value else self._get_value(column_name_fact)
        self.validate_value(column=column, value=column_value)
        if column_value:
            _condition = "{} <= '{}'".format(column, column_value) if include_equal else "{} < {}".format(column,
                                                                                                          column_value)
            self._merge_condition(_condition)
        return self
