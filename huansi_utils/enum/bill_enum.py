from enum import Enum, unique

# 单据审核方式
@unique
class HSBillAuditType(Enum):
    # 自动审核
    Auto = 0
    # 流程审核
    Flow =1
    # 手工审核
    Hand = 2
    # 自定义审核
    Custom = 3

# 单据状态
@unique
class HSBillStatus(Enum):
    # 草稿
    Draft = 0
    # 完成(提交、待审)
    Send = 1
    # 已审核
    Audit = 2
    # 作废
    Invalid = 5
    # 关闭(正常完成)
    Closed = 6

# 单据操作类型
@unique
class HSBillOperateType(Enum):
    # 保存
    AfterSave = 1
    # 送审
    SendToAudit = 2
    # 审核
    Audit = 4
    # 取消审核
    UnAudit = 8
    # 取消送审
    UnSendToAudit = 16
    # 删除前
    BeforeDelete = 32
    # 驳回
    Reject = 64
    # 打印
    Print = 128
    # 作废
    Invalid = 256
    # 取消作废
    Valid = 512
    # 关闭(正常完成)
    Close = 1024
    # 取消关闭(回到审核状态)
    UnClose = 2048

# 单据事件类型
@unique
class HSBillEvent(Enum):
    # 送审后
    SendToAudit = 2
    # 送审后
    Audit = 4
    # 取消审核后
    UnAudit = 8
    # 取消送审后
    UnSendToAudit = 16
    # 数据校验
    Validate = 64
    # 数据检验提示(送审前校验)
    ValidateHint = 128
    # 计算时间戳
    CalcTimeStamp = 256
    # 关闭
    Close = 1024
    # 取消关闭
    UnClose = 2048

# 系统单据修改模式
@unique
class HSSystemBillEditStyle(Enum):
    Full = -1
    # 只读
    Null = 0
    # 可审核(流转)
    Audit = 1
    # 可修改
    Edit = 2
    # 可删除
    Delete = 4
    # 可作废
    Invalid = 8