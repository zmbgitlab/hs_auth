from enum import Enum, unique


# 查询方式
@unique
class HSSearchMode(Enum):
    # 精确
    Sharp = 1
    # 左匹配
    Left = 2
    # 模糊
    Like = 4
    # 组合
    Combo = 8

# 数据库对象类型
@unique
class HSSqlObjectType(Enum):
    # 视图
    View = 1
    # 表
    Table = 2
    # 脚本
    Script = 3
    # 函数
    Function = 4

@unique
class HSLoadMode(Enum):
    # json_data模式
    Json = 1
    # schema模式
    Schema = 2


@unique
class HSSchemaDataModel(Enum):
    Insert = 1
    Update = 2

# schema装饰器的转换模式
@unique
class HSSchemaConvertModel(Enum):
    # 单表child模式
    Child = 1
    # header,childs模式 (save_bill)
    HdrDtl = 2