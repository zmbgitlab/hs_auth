import inspect
import traceback
from datetime import datetime
from uuid import UUID

from flask import request
from sqlalchemy.exc import OperationalError, ProgrammingError

from huansi_utils.app.apploader import logger


class HSMessage():
    def __init__(self, exception):
        self.real_error = None
        self.exception = self._get_exception(exception)
        self.original_excepiton = exception

    def _get_exception(self, exception):
        if not self.real_error:
            self.real_error = getattr(exception, 'error_code', None)
        if isinstance(exception.args[0], Exception):
            return self._get_exception(exception.args[0])
        else:
            return exception

    def insert_error_db(self, session):
        # 防止内部出现有调用rollback导致此处begin_trans时报错
        session._total_rollback_count = -1
        session.begin_trans()
        try:
            error_sql = ''
            url = request.url.split('/')[3]
            request_url = request.url
            request_info = request.get_json()
            if hasattr(self.exception, 'statement'):
                error_sql = self.exception.statement
                if hasattr(self.exception, 'params'):
                    for key, value in self.exception.params.items():
                        if value is None:
                            value = 'NULL'
                        elif isinstance(value, (str, UUID, datetime)):
                            value = "'{}'".format(value)
                        else:
                            value = str(value)
                        error_sql = error_sql.replace('%({})s'.format(key), value)
            if hasattr(self.original_excepiton, 'exception_message'):
                error = '\n'.join([str(item) for item in self.original_excepiton.exception_message])
            else:
                error = str(self.original_excepiton)
            exception = traceback.format_exc()

            sql = '''
INSERT INTO dbo.sm_log_exception 
(url,error,exception,error_sql,request_info,request_url) 
VALUES (:url,:error,:exception, :error_sql,:request_info,:request_url) 
'''
            session.exec_sql(sql, url=str(url), error=str(error), exception=str(exception), error_sql=str(error_sql),
                             request_info=str(request_info), request_url=str(request_url))

            session.commit_trans()
        except Exception as e:
            session.rollback_trans()
            logger.error('insert error_db出错\n{}'.format(str(e)))

    def format_message(self):
        if isinstance(self.exception, HSMessageException):
            error_code = self.exception.error_code
            message = self._message(str(self.exception))
            return message, error_code
        elif isinstance(self.exception, HSSQLError):
            error_message = self.exception.args[0]
            error_code = self.exception.error_code
            message = self._message(error_message)
            return message, error_code
        elif isinstance(self.exception, OperationalError) \
                or isinstance(self.exception, ProgrammingError):
            error_code = HSSQLError().error_code  # self.orgin_exception.error_code
            error_message = ''
            if getattr(self.exception, 'orig', None):
                error = []
                for error_item in self.exception.orig.args:
                    if isinstance(error_item, bytes):
                        try:
                            error_item = error_item.decode(encoding='UTF-8')
                        except UnicodeDecodeError as e:
                            error_item = str(error_item)
                    else:
                        continue
                    error.append(self._sql_message_translate(error_item, self.exception.statement))
                error_message = ','.join(error)
            elif getattr(self.exception, 'text', None):
                error_message = self.exception.text.decode(encoding='UTF-8')
            error_detail = error_message
            error_title = error_message.split('[SQL:')[0].split('[ERROR]')[0]
            message = self._message(error_title, error_detail)
            # 如果存在最外层嵌套error_code，直接取最外层
            if self.real_error:
                error_code = self.real_error
            return message, error_code
        elif isinstance(self.exception, HSException):
            error_code = 501
            message = self._message(str(self.exception))
            return message, error_code
        elif isinstance(self.exception, Exception):
            error_code = 500
            message = self._message('服务器异常:' + str(self.exception))
            return message, error_code
        else:
            error_code = 502
            message = self._message('服务器错误消息:' + str(self.exception))
            return message, error_code

    def _sql_message_translate(self, error, sql):
        '''
        sql报错信息翻译中文
        :param error:
        :param sql:
        :return:
        '''
        temp_list = error.split("'")
        if 'Invalid object name' in error:
            object_name = temp_list[1]
            return "无效对象名'{}'\n[SQL:{}]".format(object_name, sql)
        elif 'Invalid column name' in error:
            column_name = temp_list[1]
            return "无效字段名'{}'\n[SQL:{}]".format(column_name, sql)
        elif 'Incorrect syntax near' in error:
            error_path = temp_list[1]
            return "'{}'附近语法错误\n[SQL:{}]".format(error_path, sql)
        elif 'Could not find stored procedure' in error:
            error_path = temp_list[1]
            return "找不到存储过程'{}'\n[SQL:{}]".format(error_path, sql)
        # 不能在具有唯一索引“%2!”的对象“%1!”中插入重复键的行。重复键值为 %3!。
        elif 'Cannot insert duplicate key row in object' in error and 'with unique index' in error:
            error_path = self.after_string(error, 'The duplicate key value is ')
            return "数据['{}']重复'\n[SQL:{}]".format(error_path, sql)
        # 违反了 %1! 约束“%2!”。不能在对象“%3!”中插入重复键。重复键值为 %4!。
        elif 'Violation' in error and 'Cannot insert duplicate key in object' in error:
            error_path = self.after_string(error, 'The duplicate key value is ')
            return "数据['{}']重复'\n[SQL:{}]".format(error_path, sql)
        # 删除冲突 %1! 语句与 %2! 约束"%3!"冲突。该冲突发生于数据库"%4!"，表"%5!"%6!%7!%8!。
        elif 'delete' in error and 'statement conflicted with the' in error and 'The conflict occurred in database' in error:
            error_path = error
            return "数据被引用，不能删除.[ERROR:{}]'\n[SQL:{}]".format(error_path, sql)
        # 外键冲突 %1! 语句与 %2! 约束"%3!"冲突。该冲突发生于数据库"%4!"，表"%5!"%6!%7!%8!。
        elif 'FOREIGN KEY' in error and 'statement conflicted with the' in error and 'The conflict occurred in database' in error:
            error_path = error
            return "数据非法(违反外键约束).[ERROR:{}]'\n[SQL:{}]".format(error_path, sql)
        # 违反表约束 %1! 语句与 %2! 约束"%3!"冲突。该冲突发生于数据库"%4!"，表"%5!"%6!%7!%8!。
        elif 'CHECK' in error and 'statement conflicted with the' in error and 'The conflict occurred in database' in error:
            error_path = error
            return "数据非法(违反表约束).[ERROR:{}]'\n[SQL:{}]".format(error_path, sql)
        else:
            return "{}\n".format(error)

    @staticmethod
    def after_string(string: str, intercept_text: str) -> str:
        '''
        截取特定字符串之后的数据
        :param string:
        :param intercept_text:
        :return:
        '''
        if not string:
            return string
        try:
            text_index = string.index(intercept_text)
            return string[text_index + len(intercept_text):]
        except ValueError:
            return string

    def _message(self, error_title, error_detail=None, error_type=None, error_data=None):
        '''
        后端报错信息处理
        :param error_title:
        :param error_detail:
        :param error_type:
        :param error_data:
        :return:
        '''
        return {'error_title': error_title, 'error_detail': error_detail, 'error_type': error_type,
                'error_data': error_data}


class HSException(Exception):
    @property
    def error_code(self):
        return 700

    def message(self):
        return HSMessage(self).format_message()

    @staticmethod
    def get_curr_function_name():
        return inspect.stack()[1][3]

    @property
    def exception_message(self):
        return self._get_exception_message(self)

    def _get_exception_message(self, exception):
        _exception_message = []
        if not hasattr(exception, 'args'):
            return exception
        else:
            # 存储过程中文报错会以betys形式展示
            if getattr(exception, 'orig', None):
                for error_item in exception.orig.args:
                    if isinstance(error_item, Exception):
                        _exception_message += self._get_exception_message(error_item)
                    elif isinstance(error_item, bytes):
                        _exception_message.append(error_item.decode(encoding='UTF-8'))
                    else:
                        _exception_message.append(error_item)
            else:
                for error_item in exception.args:
                    if isinstance(error_item, Exception):
                        _exception_message += self._get_exception_message(error_item)
                    else:
                        _exception_message.append(error_item)
            return _exception_message


class HSArgumentError(HSException):
    '''
    参数错误，前端向后端方法传入的参数值错误
    '''

    @property
    def error_code(self):
        return 701


class HSTypeError(HSException):
    @property
    def error_code(self):
        return 702


class HSValueError(HSException):
    '''
    前端向后端传入的数据值错误(如非空字段传入空值)
    '''

    @property
    def error_code(self):
        return 703


class HSAttributeError(HSException):
    @property
    def error_code(self):
        return 704


class HSSQLError(HSException):
    # def __init__(self, e, sql):
    #     self._inner_exception = e
    #     self._sql = sql

    @property
    def error_code(self):
        return 705

    # def message(self):
    #     return 'SQL出错：'+ self.args


class HSDataError(HSException):
    @property
    def error_code(self):
        return 706


class HSNotImplementError(HSException):
    @property
    def error_code(self):
        return 707


class HSTestError(HSException):
    @property
    def error_code(self):
        return 800


class HSMessageException(HSException):
    '''
    由该方法抛出的异常,都会以状态200返回
    '''
    @property
    def error_code(self):
        return 801
