import datetime
from flask import jsonify

from huansi_utils.exception.exception import HSMessage
from huansi_utils.app.apploader import logger
from flask.wrappers import Response


# class debug(object):
#     @staticmethod
#     def print(self, *args, sep=' ', end='\n', file=None):  # known special case of print
#         # if __debug__:
#         print(*args, sep=sep, end=end, file=file)

def now():
    return datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')


def date():
    return datetime.datetime.now().strftime('%Y-%m-%d')


def message(error_title, error_detial=None, error_type=None, error_data=None):
    '''
    后端报错信息处理
    :param error_title:
    :param error_detial:
    :param error_type:
    :param error_data:
    :return:
    '''
    return {'error_title': error_title, 'error_detial': error_detial, 'error_type': error_type, 'error_data': error_data}


def succeed_message(data_obj, state_code=200, _jsonify=True):
    if not state_code:
        state_code = 200

    if isinstance(data_obj, Response):
        _jsonify = False

    if _jsonify:
        if isinstance(data_obj, tuple):
            state_code = data_obj[1]
            data_obj = jsonify(data_obj[0])
        else:
            data_obj = jsonify(data_obj)
    return data_obj, state_code

def error_message(e):
    # logger.debug(e)
    error_message, error_code = HSMessage(e).format_message()
    # if isinstance(e, HSException):
    #     error_code = e.error_code
    #     msg = error_message
    # elif isinstance(e, Exception):
    #     error_code = 500
    #     msg = '服务器异常:' + error_message
    # elif isinstance(e, HSException):
    #     error_code = 501
    #     msg = error_message
    # else:
    #     error_code = 502
    #     msg = '服务器错误消息：' + str(e)
    # logger.debug(error_message)
    logger.error(error_message)
    return jsonify(error_message), error_code
