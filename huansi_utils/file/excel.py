import os
import random
import time

import xlrd
import xlwt
from flask import make_response, send_file

from huansi_utils.exception.exception import HSException
from static_file import export_dir


def get_sheet_table_info(file_name, sheet_index=0, sheet_name=None, page_size=1000):
    '''
    获取页数、加载excel中的数据
    :param file_name: excel文件名
    :param sheet_index: sheet索引
    :param sheet_name: sheet名称
    :param page_size: 一次解析多少条excel数据
    :return: table对象
    '''
    excel = xlrd.open_workbook(file_name)
    if sheet_name:
        table = excel.sheet_by_name(sheet_name)
    elif sheet_index:
        table = excel.sheet_by_index(sheet_index)
    else:
        table = excel.sheet_by_index(0)

    # 获取表头
    fields = table.row_values(0)
    for index, field in enumerate(fields):
        fields[index] = field.replace(" ", "").replace("-", "_")

    # 获取总页数
    page_total = table.nrows // page_size
    page_total = page_total if table.nrows % page_size == 0 else page_total + 1

    return table, fields, page_total


# def load_sheet_table(file_name, sheet_index=0, sheet_name=None):
#     '''
#     加载excel中的数据
#     :param file_name: excel文件名
#     :param sheet_index: sheet索引
#     :param sheet_name: sheet名称
#     :return: table对象
#     '''
#     excel = xlrd.open_workbook(file_name)
#     if sheet_name:
#         table = excel.sheet_by_name(sheet_name)
#     elif sheet_index:
#         table = excel.sheet_by_index(sheet_index)
#     else:
#         table = excel.sheet_by_index(0)
#     return table


def load_sheet_data(field_mapper=None, fix_fields=None, table=None, page=1, page_size=1000, fields=[]) -> list:
    '''
    加载EXCEL数据

    :param file_name: EXCEL文件名
    :param sheet_name: Sheet名称
    :param sheet_index: Sheet序号，默认取第一个Sheet

    :param field_mapper: EXCEL中的第一行中的列标题与字段名映射
    :param fix_fields: 需要固定添加的列值键值对如{bill_id:100}
    :return: 返回字典集合
    '''

    if len(fields) <= 0:
        raise HSException("Excel 文档数据为空")

    row_count = table.nrows
    excel_title = fields
    if field_mapper:
        excel_title = [field_mapper[v] if v in field_mapper.keys() else v
                       for v in excel_title]

    # 初始化起始行，结束行
    start_line = 1
    end_line = row_count

    # 如果有分页，重新计算起始行结束行
    if page_size > 0:
        start_line = (page - 1) * page_size + 1
        end_line = page * page_size + 1

    list = []

    for i in range(start_line, end_line):

        # 大于等于最大行数时退出
        if i >= row_count:
            break

        # 构建一行记录数据
        line = dict(zip(excel_title, table.row_values(i)))

        line['excel_row_id'] = str(i)
        if fix_fields:
            for key, value in fix_fields.items():
                line[key] = value

        # 将一行数据存储到列表中
        list.append(line)
    return list


def export_excel(query_list=None, field_mapper=None):
    '''
    :param query_list: data_list
    :param field_mapper: dict
    :return:
    '''
    file_save_name = str(time.time()).replace('.', '') + str(random.randint(1, 10000)) + '.xls'

    real_path = os.path.join(export_dir, file_save_name)
    # 创建工作簿
    workbook = xlwt.Workbook()
    # 创建工作页名
    my_sheet = workbook.add_sheet(file_save_name, cell_overwrite_ok=True)
    # 定义日期格式
    date_format = xlwt.XFStyle()
    date_format.num_format_str = 'yyyy/mm/dd'

    # 定义excel标题
    fields_list = []
    for key, values in query_list[0].items():
        fields_list.append(key)

    field_mapper_list = []
    if field_mapper:
        for key1, values2 in field_mapper.items():
            field_mapper_list.append(key1)
        # 取交集
        title_fields = list(set(fields_list).intersection(set(field_mapper_list)))
    else:
        title_fields = fields_list

    for index, item in enumerate(title_fields):
        my_sheet.write(0, index, field_mapper[item] if field_mapper else item)

    # 定义列号和行号
    rows = 0
    num_cols = len(title_fields)

    for work_demo in query_list:
        # 循环写入数据到 my_sheet
        rows += 1
        # 序号
        my_sheet.write(rows, 0, rows)
        for cols in range(0, num_cols):
            my_sheet.write(rows, cols, work_demo[title_fields[cols]])

    workbook.save(real_path)
    file = send_file(real_path)
    response = make_response(file)
    response.headers["Content-Disposition"] = "attachment; filename={}".format(
        file_save_name.encode().decode('latin-1'))
    return response
