import os
import time

from static_file import temp_file_dir


def get_temp_file_name(file_type):
    # file_int = str(random.randint(1, 100000000))
    file_int = str(int(time.time()))
    if file_type[0:1] != '.':
        file_type = '.' + file_type
    file_name = file_int + file_type
    file_save_name = os.path.join(temp_file_dir, file_name)  # ´æ´¢Â·¾¶
    return file_save_name
