import re
from huansi_utils.app.apploader import global_validate_switch

__author__ = 'huansi'
'''
全局数据校验
'''


def global_validate(session, json_data, formula):
    if not global_validate_switch:
        return
    if not formula:
        return
    formulas = dict(formula)
    for column, formula in formulas.items():
        value = str(json_data.get(column,''))
        for formula in formula.split('|||'):
            _validate(session, column, value, formula)


def _validate(session, column, value, formula):
    if formula[:2] == '==':
        formula = formula[2:]
        Regular(column, value, formula).validate()
    elif formula[:1] in ['*', '>', '<', '='] or formula[:2] == '!=':
        Exists(column, value, formula).validate()
    elif formula == '!':
        Not_Exists(column, value, formula).validate()
    elif formula[:1] == '[' and formula[-1:] == ']':
        formula = formula[1:-1]
        Foreign_column(session, column, value, formula).validate()


# 字段存在校验
class Exists(object):
    def __init__(self, column, value, formula):
        self.column = column
        self.value = value
        self.formula = formula

    def validate(self):
        if self.formula == '*':
            if self.value == None:
                raise RuntimeError('参数[{}]不存在'.format(self.column))
        elif self.formula[:1] == '>' or self.formula[:1] == '<':
            if not self.value:
                return
            Regular(self.column, self.value, 'int').validate()
            va_str = '{}{}'.format(self.value, self.formula)
            if not eval(va_str):
                raise RuntimeError('参数[{}]没有[{}],值为[{}]'.format(self.column, self.formula, self.value))
        elif self.formula[:1] == '=':
            va_str = "'{}'=='{}'".format(self.value, self.formula[1:])
            if not eval(va_str):
                raise RuntimeError('参数[{}]只能等于[{}],值为[{}]'.format(self.column, self.formula[1:], self.value))
        elif self.formula[:2] == '!=':
            va_str = "'{}'!='{}'".format(self.value, self.formula[2:])
            if not eval(va_str):
                raise RuntimeError('参数[{}]不能等于[{}],值为[{}]'.format(self.column, self.formula[2:], self.value))


# 字段不存在校验
class Not_Exists(object):
    def __init__(self, column, value, formula):
        self.column = column
        self.value = value
        self.formula = formula

    def validate(self):
        if self.formula == '!':
            if self.value:
                raise RuntimeError('参数[{}]不能存在,值为[{}]'.format(self.column, self.value))


# 正则校验
class Regular(object):
    def __init__(self, column, value, formula):
        self.column = column
        self.value = value
        self.formula = formula

    def validate(self):
        if self.formula == 'tele':
            if not re.match(r"^(13[0-9]|14[579]|15[0-3,5-9]|16[6]|17[0135678]|18[0-9]|19[89])\d{8}$", self.value):
                raise RuntimeError('参数[{}]不是正确的手机格式,值为[{}]'.format(self.column, self.value))
        elif self.formula == 'email':
            if not re.match(
                    r"[\w!#$%&'*+/=?^_`{|}~-]+(?:\.[\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\w](?:[\w-]*[\w])?\.)+[\w](?:[\w-]*[\w])?",
                    self.value):
                raise RuntimeError('参数[{}]不是正确的邮箱格式,值为[{}]'.format(self.column, self.value))
        elif self.formula == 'str':
            if not re.match(r"^[A-Za-z]+$", self.value):
                raise RuntimeError('参数[{}]不是正确的纯字母格式,值为[{}]'.format(self.column, self.value))
        elif self.formula == 'int':
            if not re.match(r'^-?\d*\.?\d*$', self.value):
                raise RuntimeError('参数[{}]不是正确的整数或者小数格式,值为[{}]'.format(self.column, self.value))
        else:
            if not re.match(self.value, self.formula):
                raise RuntimeError('参数[{}]未能满足公式[{}],值为[{}]'.format(self.column, self.formula, self.value))


# 外键校验
class Foreign_column(object):
    def __init__(self, session, column, value, formula):
        self.column = column
        self.value = value
        self.formula = formula
        self.session = session

    def validate(self):
        table = self.formula.split('.')[0]
        column = self.formula.split('.')[1]
        va_str = '{}={}'.format(column, self.value)
        sql = '''SELECT TOP 1 1 FROM dbo.{} (NOLOCK) WHERE {}'''.format(table, va_str)
        res = self.session.exec_sql(sql)
        if res == 0:
            raise RuntimeError('参数[{}]的值不是[{}]的外键,值为[{}]'.format(self.column, self.formula, self.value))
# 参照校验

# 冗余校验

# 自定义校验
