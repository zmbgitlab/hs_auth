"""
    作用：自动生成API目录说明
    ~~~~~~~~~~~~~

    注释规范：
        说明字段获取的是类注释第一行
        负责人字段获取的是类注释第二行

        示例：
            class API:
            '''
            出库查询视图
            张三
            '''
"""


import os
import sys

BASE_DIR =os.path.dirname(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
sys.path.append(BASE_DIR)

from huansi_utils.webapi import ApiController
from flask_app import global_app



class GenerateReadMe:

    def __init__(self):
        self.dir = os.path.join(os.path.join(BASE_DIR, 'app'), 'readme.txt')
        if os.path.exists(self.dir):
            os.remove(self.dir)
        self.file = open(self.dir, 'w', encoding='utf-8')


    def get_route_rule(self):
        '''
        获取所有的路由规则
        '''
        with global_app.test_request_context():
            rules = list(ApiController()._ApiController__rules.items())
            rules.sort(key=lambda x:str(x[0]).lower())
            return rules


    def get_dir_name(self, rule):
        '''
        获取目录名
        '''
        return str(rule).split('.')[1]


    def get_file_name(self, rule):
        '''
        获取文件名
        '''
        return str(rule).split('.')[-2]


    def get_comment(self, rule):
        '''
        获取模块说明
        '''
        comments = rule.__doc__
        if comments:
            comment, author = [comment.strip() for comment in comments.splitlines()[1:3]]
            if not author:
                author = '未添加负责人'
        else:
            # print('{:<80}***  未添加注释'.format(str(rule).strip('<class> ')))
            comment = '未添加注释'
            author = '未添加负责人'
        return comment, author


    def get_view_class(self, rule):
        '''
        获取视图类
        '''
        return str(rule).split('.')[-1].strip("'>")


    def write_table_head(self):
        '''
        写表头
        '''
        self.file.write('{:<24}{:<31}{:<32}{:<23}{:<5}\n'.format('目录', '文件名', '视图类', '说明', '负责人'))
        self.file.write('-' * 120 + '\n')


    def write_file(self, dir_name, file_name, view_class, comment, author):
        '''
        写主体内容
        '''
        # 此处解决了中英文混合输入带来的对齐问题！！！
        self.file.write('{:<25}|{:<32}|{:<33}|{:<{blank}}\t|{:>5}\n'
                        .format(dir_name, file_name, view_class, comment, author,
                                blank=22-len(comment.encode('GBK'))+len(comment)))

    def run(self):
        '''
        主函数
        '''
        print('生成中...')
        rules = self.get_route_rule()
        self.write_table_head()
        for rule in rules:
            rule = rule[0]
            dir_name = self.get_dir_name(rule)
            file_name = self.get_file_name(rule)
            view_class = self.get_view_class(rule)
            comment, author = self.get_comment(rule)
            self.write_file(dir_name, file_name, view_class, comment, author)
        self.file.write('-' * 120 + '\n')
        self.file.close()


if __name__ == '__main__':
    generate_read_me = GenerateReadMe()
    generate_read_me.run()
    print('完成')
