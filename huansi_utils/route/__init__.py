from collections import namedtuple

RouteTuple = namedtuple("RouteTuple", ["rule", "endpoint", "fn", "options"])


class HSRouteRegister:

    def __init__(self):
        self.routes = []

    def init_app(self, app):
        for route_info in self.routes:
            app.add_url_rule(route_info.rule, route_info.endpoint, route_info.fn, **route_info.options)

    @staticmethod
    def get_original_func(fn):
        if not getattr(fn, '__wrapped__', None):
            return fn
        temp_fn = fn
        while (True):
            temp_fn = temp_fn.__wrapped__
            if not getattr(temp_fn, '__wrapped__', None):
                return temp_fn

    def register_route(self, **options):
        def decorator(fn):
            # for var_name in getargs(fn).:
            endpoint = options.pop('endpoint', None)
            params = options.pop('params', None)
            original_func = self.get_original_func(fn) or fn
            if not params:
                i = 0
                params = ''
                while i < original_func.__code__.co_argcount:
                    var_name = original_func.__code__.co_varnames[i]
                    var_type = 'int' if var_name[-2:] == 'id' else 'string'
                    params += '<{0}:{1}>/'.format(var_type, var_name)
                    i += 1
            fun_name = original_func.__name__
            rule = '/' + str.replace(fun_name, '__', '/')
            if rule[:-1] != '/':
                rule += '/'
            if params:
                rule += params
            # print('register_route.rule:', rule)

            self.routes.append(RouteTuple(rule=rule, endpoint=endpoint, fn=fn, options=options))
            return fn

        return decorator


_route_register_instance = HSRouteRegister()
# 导出供外部作为装饰器使用
register_route = _route_register_instance.register_route
