import math
import os
from collections import Iterable
from urllib.parse import quote, unquote
from xml.etree import ElementTree
from xml.etree.ElementTree import Element
from xml.etree.ElementTree import SubElement

import xlwt
from flask import g, request, make_response, send_file

from huansi_utils.common.schema import schema_data, schema_data_list
from huansi_utils.common.string import str_to_int, before_string, after_string
from huansi_utils.db.db import new_id
from huansi_utils.enum.enum import HSLoadMode
from huansi_utils.exception.exception import HSNotImplementError, HSArgumentError, HSException
from huansi_utils.file.excel import load_sheet_data, get_sheet_table_info
from huansi_utils.file.file import get_temp_file_name
from huansi_utils.global_validate import global_validate
from huansi_utils.server.server import HSBaseService, HSTableService
from static_file import excel_file_dir


class HSBaseUCService(HSBaseService):
    def __init__(self, session=None):
        if session is None:
            session = g.hs_session
        super().__init__(session=session)

    def _import_dict_data(self, dict_data, import_sql, temp_table='#Excel'):
        '''
        将多行字典数据，转为临时表
        :param data:
        :param temp_table:
        :return:
        '''
        if len(dict_data) <= 0:
            raise HSException("Excel 文档数据为空")

        fields = list(dict_data[0].keys())

        # 串联字段
        field_insert_str = "[" + "],[".join(fields) + "]"
        field_row_str = "'{" + "}','{".join(fields) + "}'"

        # 准备插入所需 Values 部分
        row_values = "(" + field_row_str + ")"
        values_sql_list_str = []
        for row in dict_data[:]:  # type: dict 
            for key, item in row.items():
                if isinstance(item, float) and item % 1 == 0.0:  # type:float
                    row[key] = int(item)
            values_sql_list_str.append(row_values.format(**row))
        values_sql_str = ",\n".join(values_sql_list_str)

        # 创建插入语句SQL
        insert_sql = """INSERT INTO {0}(""".format(temp_table) + field_insert_str + """)\n SELECT * FROM (VALUES"""
        insert_sql += values_sql_str + ")A\n("
        insert_sql += field_insert_str.format(temp_table) + ")\n"

        return insert_sql + import_sql + '''\n delete from {0}'''.format(temp_table)

    def _import_excel(self, file, import_sql, temp_table="#Excel" \
                      , sheet_name=None, sheet_index=0, field_mapper=None, fix_fields=None):

        if isinstance(file, str):
            file_name = file
        else:
            file_name = get_temp_file_name('xls')
            file.save(file_name)
        try:
            # import_sql中若没有EXEC，则为bill_type
            if import_sql.lower().count("exec dbo.") == 0:
                import_sql = "EXEC dbo.sppbImportExcel @sBillType='%s'" % import_sql
                sql = """ SELECT OBJECT_ID('sppbImportExcel') """
            else:
                # 兼容 EXEC
                sql_obj_name = before_string(after_string(import_sql.lower(), "exec dbo."), ' ')

                if not sql_obj_name or len(sql_obj_name) <= 0:
                    raise HSException(import_sql + "错误")

                sql = """   SELECT TOP 1 1
                            FROM dbo.dvParameters A
                            WHERE A.sObjectName='%s' AND A.sName='sXMLData'
                                """ % sql_obj_name

            # model_flag True: xml模式，False:临时表模式
            if self.retrive_sql(sql):
                model_flag = True
            else:
                model_flag = False

            table, fields, page_total = get_sheet_table_info(file_name=file_name, sheet_name=sheet_name,
                                                             sheet_index=sheet_index)
            xml_sql_list = []
            for page in range(1, page_total + 1):
                data = load_sheet_data(field_mapper=field_mapper, fix_fields=fix_fields, table=table, page=page,
                                       fields=fields)

                if model_flag:
                    xml_sql_list.append(self._import_excel_to_xml(data, import_sql))
                else:
                    xml_sql_list.append(
                        self._import_dict_data(data, import_sql=import_sql, temp_table=temp_table))

            # 如果是临时表模式
            if not model_flag:
                fields.append("excel_row_id")

                # 创建临时表SQL
                field_create_str = "[" + "] nvarchar(max),[".join(fields) + "] nvarchar(max)"
                create_sql = "CREATE TABLE {0}(".format(temp_table) + "\n"
                create_sql = create_sql + field_create_str + ")" + "\n"
                xml_sql_list.insert(0, create_sql)

                # drop temp_table
                drop_table = "drop table " + temp_table
                xml_sql_list.append(drop_table)

            if len(xml_sql_list):
                sql = " \n\n\n ".join(xml_sql_list)
                self.exec_sql(sql)
        finally:
            os.remove(file_name)

    def _import_excel_to_xml(self, data: list, import_sql):
        """
        导入excel，将得到的数据转化为xml

        :param data: list<dict> excel经过转化得到的列表数据
        :param import_sql: 要执行的SQL
        :return:
        """
        root_element = Element("Table")
        for row in data[:]:  # type: dict
            tmp_row = {}
            for key, item in row.items():
                if item == '':
                    continue
                if isinstance(item, float) and item == math.floor(item):  # type:float
                    item = int(item)
                elif isinstance(item, float):  # type:float
                    item = str(item)
                tmp_row[key] = str(item)
            SubElement(root_element, "row", attrib=tmp_row)

        tree_xml = ElementTree.tostring(root_element, encoding="UTF-8").decode("UTF-8")

        if import_sql.count('@'):
            sql = import_sql + """,@sXMLData='%s' """ % tree_xml
        else:
            sql = import_sql + """ @sXMLData='%s' """ % tree_xml
        if __debug__:
            print('insert_sql:\n', sql)
        return sql

    def _export_excel(self, type):
        """
        导出EXCEL

        :param type: 类型
        :return: 文件名（文件存储在/static_file/temp路径下）
        """
        # 通过存储过程获取数据

        if not type or type == "null" or type.strip() == "":
            raise HSException("请求参数出错")

        data = self.query_sql("""
                    DECLARE @sFileName NVARCHAR(100)=0x
                    EXEC dbo.sppbExportExcel @sBillType='{}',@sFileName=@sFileName OUTPUT
                    Select sFileName=@sFileName
                    """.format(type), many=True)  # type:list

        # 没有数据就返回模板
        if not data['set1']:
            full_file_name = os.path.join(excel_file_dir, unquote(data["set2"][0]['sFileName']))
            response = make_response(send_file(full_file_name))
            response.headers["Content-Disposition"] = "attachment; filename={}".format(quote(data["set2"][0]['sFileName']))
            return response

        if not data["set2"] or len(data["set2"]) <= 0:
            raise HSException("数据库出错")

        export_file_name = data["set2"][0]['sFileName']
        export_data = data["set1"]
        # 将数据通过xlwt 写入文件
        workbook = xlwt.Workbook(encoding="UTF-8")
        worksheet = workbook.add_sheet("My Worksheet")

        for row in range(len(export_data) + 1):
            for column_index, column_key in enumerate(list(export_data[0].keys())):
                if row == 0:
                    worksheet.write(0, column_index, label=column_key)
                else:
                    worksheet.write(row, column_index, label=export_data[row - 1][column_key])

        temp_file_name = get_temp_file_name('xlsx')
        workbook.save(temp_file_name)
        response = make_response(send_file(temp_file_name))
        # response = make_response(workbook.get_biff_data())
        response.headers["Content-Disposition"] = "attachment; filename={}".format(quote(export_file_name))
        return response


class HSSingleUCService(HSBaseUCService):
    def __init__(self, session=None):
        if session is None:
            session = g.hs_session
        super().__init__(session=session)
        self.service = HSTableService(self.session, self.model_class)

    @property
    def bill_type(self):
        '''
        单据类型
        @property
        def bill_type(self):
            return 54
        :return:
        '''
        # raise RuntimeError('未定义单据类型')
        return 0

    @property
    def table_name(self):
        '''
        获取表名
        :return:
        '''
        return self.model_class.__tablename__

    def _global_validate(self, json_data, type, model_name=None):
        '''
        json数据校验
        :param json_data: json数据
        :param type: 校验类型，由此类型获取校验公式
        :return: 校验失败，抛出异常
        '''
        formula = self.global_validate_data(type, model_name)
        if json_data and formula:
            return global_validate(self.session, json_data, formula)

    def query_one(self, id, dump_data=True):
        return self.service.query_one(id, dump_data)

    def save_one(self, json_data, insert=None, retrive=True):
        '''
        保存(新增/修改)单行数据
        :param json_data:
        :param insert:
        :param send_bill:
        :return:
        '''
        if isinstance(json_data, dict):
            id = json_data.get('id')
        else:
            id = json_data.key
        schema_data = None
        if insert is None:
            insert = id is None

        if insert and id is None:
            id = new_id()

        if isinstance(json_data, dict):
            if self.schema_class:
                if insert:
                    schema_data = self.schema_class().load_add_data(json_data)
                    schema_data.key = id
                else:
                    schema_data = self.schema_class().load_update_data(json_data)
            else:
                # 对传入的json数据进行校验
                if insert:
                    json_data['id'] = id
                    self._global_validate(json_data=json_data, type='add')
                else:
                    self._global_validate(json_data=json_data, type='update')
        else:
            schema_data = json_data

        self._before_save_one(json_data=json_data, insert=insert, schema_data=schema_data)
        data = self.service.save_one(json_data=json_data, insert=insert, send_bill=False, schema_data=schema_data,
                                     retrive=retrive)
        self._after_save(data.get('id'))
        return data

    def _after_save(self, id):
        if self.bill_type > 0:
            # after save操作
            sql = 'exec dbo.sppbBillOperate_AfterSave @iBillTypeId={},@iBillId={}'.format(self.bill_type, id)
            self.exec_sql(sql=sql)

    def save_list(self, json_data, insert=None, retrive=True):
        '''
        保存(新增/修改)多行数据
        :param json_data: 前端传入的 Json对象(含多行数据)
        :return: 所有修改行的最新数据
        '''
        result = []
        if isinstance(json_data, schema_data_list):
            json_data = json_data.data
        for json in json_data:
            data = self.save_one(json, insert, retrive=retrive)
            result.append(data)
        return result

    def _before_save_one(self, json_data, insert=True, schema_data=None):
        '''
        保存之前操作
        :param model:
        :param insert:
        :return:
        '''
        pass

    def _before_delete_one(self, id):
        '''
        删除前事件
        :param id: 要删除的主键ID
        '''
        pass

    def delete_one(self, id):
        self._before_delete_one(id)
        if self.bill_type > 0:
            sql = '''EXEC dbo.sppbBillOperate_Delete @iBillTypeId={},@sBillId='{}',@iUserId={}''' \
                .format(int(self.bill_type), id, 0)
            return self.exec_sql(sql)
        else:
            self.service.delete_one(id=id)

    def _query_list(self, args):
        '''
        query_list的具体实现，由子类继承实现

        per_page = int(args.get('per_page', 10))

        where = HSConditionCalc(args)
        .quick_query(IM_arrive.bill_no, IM_arrive.style_code)
        .where(M_arrive.bill_status != 5)
        .where_equal(IM_arrive.bill_status, IM_arrive.customer_id)
        .where_like(IM_arrive.bill_no, IM_arrive.style_code, IM_arrive.order_no)
        .condition

        args=HSArgCalc(args)
        where = and_(
            # IM_arrive.bill_status != 5,
            # args.equal(IM_arrive.bill_status, IM_arrive.customer_id),
            # args.like(IM_arrive.bill_no, IM_arrive.style_code, IM_arrive.order_no),
        )

        query = self._new_query().filter(where)
        return query, per_page

        :param args: Request.args动态查询条件
        :return: 查询结果，ORM.query或是sql语句, per_page每页行数
        '''
        raise HSNotImplementError('_query_list')

    def query_list(self, args):
        print('query_list')
        return self.service._query(self._query_list, args)

    def delete_list(self, id_list):
        return self.service.delete_list(id_list)

    def last_order(self, bill_no):
        '''
        上一条
        :param bill_no:
        :return:
        '''
        return self._get_sort_order(bill_no, direction='last')

    def next_order(self, bill_no):
        '''
        下一条
        :param bill_no:
        :return:
        '''
        return self._get_sort_order(bill_no, direction='next')

    def _get_sort_order(self, bill_no, direction='next'):
        if not direction:
            return
        if direction == 'last':
            result = self._new_query() \
                .filter(self.model_class.bill_no < bill_no) \
                .order_by(self.model_class.bill_no.desc()) \
                .first()
        elif direction == 'next':
            result = self._new_query() \
                .filter(self.model_class.bill_no > bill_no) \
                .order_by(self.model_class.bill_no) \
                .first()
        return HSTableService(self.session, self.model_class)._query_result(result)


class HSHdrDtlUCService(HSSingleUCService):
    def __init__(self, session=None):
        if session is None:
            session = g.hs_session
        super().__init__(session=session)
        # 注册当前所有的model信息
        self.register_model_to_dict()
        # 注册当前所有的service信息
        self.register_service_to_dict()
        # 注册当前所有的schema信息
        self.register_schema_to_dict()

    @property
    def table_name(self):
        '''
        获取表名
        :return:
        '''
        return self.hdr_model_class.__tablename__

    @property
    def model_class(self):
        return self.hdr_model_class

    @property
    def hdr_model_class(self):
        '''
        头表的模型
        @property
        def hdr_model_class(self):
            return IM_in
        :return:
        '''
        raise RuntimeError('未定义头表model')
        pass

    @property
    def dtl_model_class(self):
        '''
        明细表的模型
        @property
        def hdr_model_class(self):
            return {'im_in_item':IM_in_item,'im_in_item_serial':IM_in_item_serial}
            or
            return IM_in_item,IM_in_item_serial
        :return:
        '''
        raise RuntimeError('未定义明细表model')
        pass

    @property
    def bill_type(self):
        '''
        单据类型
        @property
        def bill_type(self):
            return 54
        :return:
        '''
        raise RuntimeError('未定义单据类型')

    @property
    def schema_class(self):
        return self.hdr_schema_class

    @property
    def hdr_schema_class(self):
        '''
        def hdr_schema_class(self):
            return IM_arrive_schema
        return None
        :return:
        '''
        return None

    @property
    def dtl_schema_class(self):
        '''
        def dtl_schema_class(self):
            return IM_arrive_schema,IM_arrive_item_schema
        :return:
        '''
        return None

    def register_model_to_dict(self):
        '''
        注册当前所有的model信息
        :return:
        '''
        model_dict = {}
        model_dict[str(self.hdr_model_class.__tablename__)] = self.hdr_model_class
        if isinstance(self.dtl_model_class, Iterable):
            if isinstance(self.dtl_model_class, dict):
                for model_name, model in self.dtl_model_class.items():
                    model_dict[model_name] = model
            else:
                for model in self.dtl_model_class:
                    model_dict[str(model.__tablename__)] = model
        else:
            model_dict[str(self.dtl_model_class.__tablename__)] = self.dtl_model_class

        self.model_dict = model_dict

    def register_service_to_dict(self):
        '''
        注册当前所有的service信息
        :return:
        '''
        service_dict = {}
        service_dict[str(self.hdr_model_class.__tablename__)] = HSTableService(self.session, self.model_class)
        if isinstance(self.dtl_model_class, Iterable):
            if isinstance(self.dtl_model_class, dict):
                for model_name, model in self.dtl_model_class.items():
                    service_dict[model_name] = HSTableService(self.session, model)
            else:
                for model in self.dtl_model_class:
                    service_dict[str(model.__tablename__)] = HSTableService(self.session, model)
        else:
            service_dict[str(self.dtl_model_class.__tablename__)] = HSTableService(self.session, self.dtl_model_class)

        self.service_dict = service_dict

    def register_schema_to_dict(self):
        '''
        注册当前所有的schema信息
        :return:
        '''
        schema_dict = {}
        if not self.hdr_schema_class:
            self.schema_dict = schema_dict
            return
        schema_dict[str(self.hdr_schema_class.__tablename__)] = self.hdr_schema_class()
        if isinstance(self.dtl_schema_class, Iterable):
            if isinstance(self.dtl_schema_class, dict):
                for schema_name, schema in self.dtl_schema_class.items():
                    schema_dict[schema_name] = schema()
            else:
                for schema in self.dtl_schema_class:
                    schema_dict[str(schema.__tablename__)] = schema()
        else:
            schema_dict[str(self.dtl_schema_class.__tablename__)] = self.dtl_schema_class()

        self.schema_dict = schema_dict

    def query_bill(self, id):
        '''
        查询单条记录详细信息
        :param id: 要查询的行的主键id
        :return: json数据包
        '''
        print('query_bill')
        id = str_to_int(id)
        if id is None:
            raise HSArgumentError('id不能为空')

        hdr_model = self._query_hdr(id).first()
        model_name = self.hdr_model_class.__tablename__
        if not hdr_model:
            raise HSException('当前单据[{}]不存在'.format(str(id)))
        hdr_result = self._get_service_from_modelname(model_name).dump_data(hdr_model, many=False)
        hdr_result = {"table_name": model_name, "data": hdr_result}

        childs = []
        for model_name, model in self.model_dict.items():
            if model == self.model_class:
                continue

            dtl_query = self._query_dtl_list(model_name, id)
            if dtl_query == '':
                dtl_query = self._new_query_columns(model).filter_by(bill_id=id)
            elif dtl_query == None:
                continue
            per_page = request.args.get('per_page')
            page = request.args.get('page')
            # 分页
            if per_page and page:
                # 分页需要排序，默认给一个按id正序排序
                dtl_query = dtl_query.order_by(self._get_model_from_modelname(model_name).id)
                dtl_page_result = self._query_result(query=dtl_query, per_page=per_page, page=page)
                dtl_result = {"table_name": model_name, "data": dtl_page_result['table'],
                              "paging": dtl_page_result['paging']}
            # 不分页
            else:
                dtl_result = self._get_service_from_modelname(model_name).dump_data(dtl_query, many=True)
                dtl_result = {"table_name": model_name, "data": dtl_result}
            childs.append(dtl_result)

        result = {"header": hdr_result, "childs": childs}
        return result

    def _query_hdr(self, id):
        '''
        头表查询(可被子类继承实现对应的业务代码)
        def _query_hdr(self, id):
            return self._new_query_columns(IM_stock_check, IM_store.store_name) \
                .join(IM_store, IM_store.id == IM_stock_check.store_id) \
                .filter(IM_stock_check.id == id)
        :param model:
        :param id:
        :return:
        '''
        return self._new_query_columns(self.hdr_model_class).filter_by(id=id)

    def _query_dtl_list(self, model_name, bill_id):
        '''
        明细表查询(可被子类继承实现对应的业务代码)
        多个明细查询，可以如下
        def _query_dtl_list(self, model_name, bill_id):
            if model_name == 'im_stock_check_item_serial':
                return self._new_query_columns(IM_stock_check_item_serial,
                                               MM_material_serial.quantity_unit_name,
                                               MM_material_serial.product_line). \
                    join(MM_material_serial, model.material_serial_id == MM_material_serial.id) \
                    .filter(IM_stock_check_item_serial.bill_id == bill_id)
        :param model_name:
        :param bill_id:
        :return:返回空'' 默认全表查询;返回None 不查询;返回BaseQuery 自定义查询
        '''
        return ''

    def get_save_bill_schema_data(self, json_data):
        class _data():
            pass

        hdr_json_data = json_data['header']
        dtl_json_data = json_data['childs']

        hdr_name = hdr_json_data['table_name']

        hdr_data = _data()
        setattr(hdr_data, 'table_name', hdr_name)
        setattr(hdr_data, 'data', self._get_schema_from_modelname(hdr_name).new_object(hdr_json_data['data']))

        dtl_data_list = []
        for child in dtl_json_data:
            dtl_item_data = _data()
            setattr(dtl_item_data, 'table_name', child['table_name'])
            setattr(dtl_item_data, 'data',
                    self._get_schema_from_modelname(child['table_name']).new_object(data=child['data'], many=True))
            dtl_data_list.append(dtl_item_data)

        data = _data()
        setattr(data, 'header', hdr_data)
        setattr(data, 'childs', dtl_data_list)

        return data

    def _save_bill_schema(self, schema_data, send_bill=False, insert=None, retrive=True):
        hdr_name = schema_data.__tablename__

        # 计算insert和bill_id
        bill_id = schema_data.key
        bill_id = bill_id if bill_id is None else int(bill_id)
        if insert is None:
            insert = bill_id is None
        if insert and bill_id is None:
            bill_id = new_id()

        # bill_id回写
        if insert:
            schema_data.key = bill_id

        # 整单校验
        self._before_save_bill(header_data=schema_data)

        # model_data = self._new_query_model(IM_arrive_item).filter_by(bill_id=1111).all()
        # im_in_item = IM_in_item_schema().new_object(many=True)
        #
        # im_in_item.load(model_data,update_Status=3,field_map={'iIden':'id'},key_field_map={})
        # if im_in_item.sum('quantity')>100:
        #     pass
        # s = im_in_item.where(lambda id: id > 10).join_text('id')
        # im_in_uc_service.save(im_in_item)

        # 获取单据状态
        if getattr(self.hdr_schema_class(), '__keyfield__', None) == 'iIden':
            bill_data = self.hdr_schema_class().new_object(id=bill_id).retrive_fields('iBillStatus')
        else:
            bill_data = self.hdr_schema_class().new_object(id=bill_id).retrive_fields('bill_status')
        if bill_data:
            if getattr(self.hdr_schema_class(), '__keyfield__', None) == 'iIden':
                bill_status = bill_data.iBillStatus
            else:
                bill_status = bill_data.bill_status
            if bill_status == 1:
                HSGlobalBillService(self.session, self.bill_type).un_send_bill(bill_id, 0)
            elif bill_status == 2:
                raise HSException('当前单据已审核,不能修改')

        # 保存表头
        self._save_table(json_data=None, model_name=hdr_name, insert=insert,
                         schema_data=schema_data, retrive=retrive)

        # 明细数据
        for child in schema_data._childs:
            if child is None:
                break
            child_name = child.__tablename__
            for dtl_data_item in child.data:
                id = dtl_data_item.key
                dtl_data_item.billId = bill_id
                update_Status = dtl_data_item.iUpdateStatus
                if update_Status == 1:  # 新增
                    self._save_table(json_data=None, model_name=child_name, insert=True,
                                     schema_data=dtl_data_item, retrive=False)
                elif update_Status == 2:  # 修改
                    self._save_table(json_data=None, model_name=child_name, insert=False,
                                     schema_data=dtl_data_item, retrive=False)
                elif update_Status == 4:  # 删除
                    self._before_save_one(json_data=None, model_name=child_name,
                                          schema_data=dtl_data_item)
                    self._get_service_from_modelname(child_name).delete_one(id)
        self.flush()
        # 保存并送审
        if send_bill:
            if send_bill.lower() == 'true':
                HSGlobalBillService(self.session, self.bill_type).send_bill(bill_id, 0)

        # 是否保存之后重新查询
        retrieve = request.args.get('retrieve', 'true')
        if retrieve.lower() == 'true':
            return self.query_bill(bill_id)

    def save_bill(self, json_data, send_bill=None, insert=None, retrive=True):
        '''
         保存
         {
             "header":{
                 "table_name":im_in_item,
                 "data":{
                     "a":1
                 }
             },
             childs:[
             {
                 "table_name":im_in_item,
                 "data":[{
                         "a":123
                         "iUpdateStatus":1   --新增
                     },
                     {
                         "id"11111
                         "a":2323
                         "iUpdateStatus":2  --修改
                     },
                     {
                         "id":111
                         "iUpdateStatus":4  --删除
                     }]
             },
             {
                 "table_name":IM_in_item_serial,
                 "data":[{
                         "a":123
                         "iUpdateStatus":1   --新增
                     },
                     {
                         "id"11111
                         "a":2323
                         "iUpdateStatus":2  --修改
                     },
                     {
                         "id":111
                         "iUpdateStatus":4  --删除
                     }]
             }
             ]
         }
         :return:
         '''
        # 判断数据是否为schema_data
        if isinstance(json_data, schema_data):
            return self._save_bill_schema(schema_data=json_data, send_bill=send_bill, insert=insert, retrive=retrive)

        # 获取表头和明细数据
        header = json_data.get('header', '')
        childs = json_data.get('childs', '')

        # 获取数据并返回类型(schema还是json_data)
        model_name = header.get('table_name')
        header_json_data = header.get('data')
        header_info = self._get_data_from_json_data(header_json_data, model_name, many=False)

        # 计算insert和bill_id
        bill_id = int(header_json_data.get('id', 0))
        if insert is None:
            insert = bill_id == 0
        if insert and bill_id == 0:
            bill_id = new_id()

        dtl_info_list = []
        # 明细数据
        if childs:
            for child in childs:
                model_name = child.get('table_name')
                child_data = child.get('data')
                dtl_info_list.append(self._get_data_from_json_data(child_data, model_name, many=True))

        # 头表数据
        hdr_schema_data = None
        header_type = header_info['type']
        header_model_name = header_info['model_name']
        if header_type == HSLoadMode.Schema:
            hdr_schema_data = header_info['data']
            hdr_schema_data.key = bill_id

        # bill_id回写
        if insert:
            header_json_data['id'] = bill_id

        # 整单校验
        self._before_save_bill(header_data=header_info, dtl_data=dtl_info_list)

        # 获取单据状态
        data = self.query_one(bill_id)
        bill_status = int(data.get('bill_status', 0)) if data else 0
        if bill_status == 1:
            HSGlobalBillService(self.session, self.bill_type).un_send_bill(bill_id, 0)
        elif bill_status == 2:
            raise HSException('当前单据已审核,不能修改')

        # 保存表头
        data = self._save_table(json_data=header_json_data, model_name=header_model_name, insert=insert,
                                schema_data=hdr_schema_data, retrive=retrive)
        bill_id = int(data.get('id'))

        # 明细数据
        if dtl_info_list:
            for dtl_info in dtl_info_list:
                child_type = dtl_info['type']
                child_model_name = dtl_info['model_name']
                for dtl_data_item in dtl_info['data']:
                    item_json_data = None
                    item_schema_data = None
                    if child_type == HSLoadMode.Schema:
                        item_schema_data = dtl_data_item
                        id = item_schema_data.key
                        item_schema_data.bill_id = bill_id
                        update_Status = item_schema_data.iUpdateStatus
                    elif child_type == HSLoadMode.Json:
                        item_json_data = dtl_data_item
                        id = item_json_data.get('id')
                        item_json_data['bill_id'] = bill_id
                        update_Status = item_json_data.get('iUpdateStatus')
                    if update_Status == 1:  # 新增
                        self._save_table(json_data=item_json_data, model_name=child_model_name, insert=True,
                                         schema_data=item_schema_data, retrive=False)
                    elif update_Status == 2:  # 修改
                        self._save_table(json_data=item_json_data, model_name=child_model_name, insert=False,
                                         schema_data=item_schema_data, retrive=False)
                    elif update_Status == 4:  # 删除
                        self._before_save_one(json_data=item_json_data, model_name=child_model_name,
                                              schema_data=item_schema_data)
                        self._get_service_from_modelname(model_name).delete_one(id)

        self._after_save(bill_id)
        self.flush()
        # 保存并送审
        if send_bill:
            if send_bill.lower() == 'true':
                HSGlobalBillService(self.session, self.bill_type).send_bill(bill_id, 0)

        # 是否保存之后重新查询
        retrieve = request.args.get('retrieve', 'true')
        if retrieve.lower() == 'true':
            return self.query_bill(bill_id)

    def delete_list(self, id_list):
        for id in id_list:
            self.delete_one(id)

    def _save_table(self, json_data, model_name, insert, schema_data=None, retrive=True):
        '''
        保存
        :param json_data:
        :param model_name:
        :param insert:
        :return:
        '''
        schema_class = self._get_schema_from_modelname(model_name)
        if not schema_class:
            if insert:
                self._global_validate(json_data=json_data, type='add', model_name=model_name)
            else:
                self._global_validate(json_data=json_data, type='update', model_name=model_name)
        self._before_save_one(json_data=json_data, model_name=model_name, insert=insert, schema_data=schema_data)
        return self._get_service_from_modelname(model_name).save_one(json_data=json_data, insert=insert,
                                                                     schema_data=schema_data, retrive=retrive)

    def _before_save_one(self, json_data, model_name=None, insert=True, schema_data=None):
        pass

    def _before_save_bill(self, header_data=None, dtl_data=None):
        '''
        整单校验
        :param json_data:
        :return:
        '''
        pass

    def re_write(self, bill_id, *args):
        pass

    def _get_sort_order(self, bill_no, direction='next'):
        if not direction:
            return
        if direction == 'last':
            result = self._new_query() \
                .filter(self.model_class.bill_no < bill_no) \
                .order_by(self.model_class.bill_no.desc()) \
                .first()
        elif direction == 'next':
            result = self._new_query() \
                .filter(self.model_class.bill_no > bill_no) \
                .order_by(self.model_class.bill_no) \
                .first()
        if not result:
            return
        return self.query_bill(result.id)

    def _get_model_from_modelname(self, model_name):
        '''
        通过表名获取表的model
        :param model_name:表名
        :return:
        '''
        if not isinstance(model_name, str) and model_name is not None:
            raise HSException('model_name必须是表名字符串类型')
        if not self.model_dict:
            return None
        if model_name is None:
            if isinstance(self.dtl_model_class, Iterable):
                return self.dtl_model_class[0]
            else:
                return self.dtl_model_class
        model = self.model_dict.get(model_name)
        if not model:
            raise RuntimeError('没有找到对应的model')
        return model

    def _get_schema_from_modelname(self, model_name):
        '''
        通过model_name获取schema
        :param model:
        :return:
        '''
        if not isinstance(model_name, str) and model_name is not None:
            raise HSException('model_name必须是表名字符串类型')
        if not self.schema_dict:
            return None
        if model_name is None:
            if isinstance(self.dtl_schema_class, Iterable):
                return self.dtl_schema_class[0]()
            else:
                return self.dtl_schema_class()
        model = self.schema_dict.get(model_name)
        return model

    def _get_service_from_modelname(self, model_name):
        '''
        通过表名获取service
        :param model_name:
        :return:
        '''
        if not isinstance(model_name, str) and model_name is not None:
            raise HSException('model_name必须是表名字符串类型')
        if not self.service_dict:
            return None
        if model_name is None:
            if isinstance(self.dtl_model_class, Iterable):
                model_name = self.dtl_model_class[0].__tablename__
            else:
                model_name = self.dtl_model_class.__tablename__
        service = self.service_dict.get(model_name)
        if not service:
            raise RuntimeError('没有找到对应的service')
        return service

    def query_bill_dtl_list(self, bill_id, model_name=None):
        model_class = self._get_model_from_modelname(model_name)
        model = self._new_query_columns(model_class).filter_by(bill_id=bill_id).all()
        return self._get_service_from_modelname(model_name).dump_data(model, many=True)

    def query_bill_dtl_one(self, id, model_name=None):
        model_class = self._get_model_from_modelname(model_name)
        model = self._new_query_columns(model_class).filter_by(id=id).first()
        return self._get_service_from_modelname(model_name).dump_data(model, many=False)

    def query_bill_dtl_by(self, model_name=None, **kwargs):
        '''
        查询返回多行数据
        :param kwargs: 查询条件，如id=id
        :return: query对象
        '''
        model_class = self._get_model_from_modelname(model_name)
        return self._new_query_columns(model_class).filter_by(**kwargs)

    def save_bill_dtl_list(self, json_data, model_name=None, insert=None, retrive=True):
        '''
        保存(新增/修改)多行数据
        :param json_data: 前端传入的 Json对象(含多行数据)
        :return: 所有修改行的最新数据
        '''
        result = []
        if isinstance(json_data, schema_data_list):
            json_data = json_data.data
        for json in json_data:
            data = self.save_bill_dtl_one(json, model_name, insert, retrive=retrive)
            result.append(data)
        return result

    def save_bill_dtl_one(self, json_data, model_name=None, insert=None, retrive=True):
        '''
        保存 单条数据
        :param json_data:
        :param model_name:
        :param insert:
        :return:
        '''
        if isinstance(json_data, dict):
            id = json_data.get('id')
        else:
            id = json_data.key
        schema_data = None
        if insert is None:
            insert = id is None

        if insert and id is None:
            id = new_id()

        if isinstance(json_data, dict):
            if self.schema_class:
                if insert:
                    schema_data = self._get_schema_from_modelname(model_name).load_add_data(json_data)
                    schema_data.key = id
                else:
                    schema_data = self._get_schema_from_modelname(model_name).load_update_data(json_data)
            else:
                # 对传入的json数据进行校验
                if insert:
                    json_data['id'] = id
                    self._global_validate(json_data=json_data, type='add')
                else:
                    self._global_validate(json_data=json_data, type='update')
        else:
            schema_data = json_data
        return self._save_table(json_data, model_name, insert, schema_data=schema_data, retrive=retrive)

    def _get_data_from_json_data(self, json_data, model_name, many=False):
        '''
        通过json_data 获取数据
        如果schema存在返回对象
        不存在返回json_data
        :param json_data:
        :param model_name:
        :return:
        '''
        if not json_data:
            raise HSException('数据不能为空')
        schema = self._get_schema_from_modelname(model_name)
        if schema:
            type = HSLoadMode.Schema
            # 多条数据的时候
            if many:
                data = []
                for item_data in json_data:
                    if not item_data.get('id'):
                        data.append(schema.load_add_data(item_data))
                    else:
                        data.append(schema.load_update_data(item_data))
            else:
                if not json_data.get('id'):
                    data = schema.load_add_data(json_data)
                else:
                    data = schema.load_update_data(json_data)
        else:
            data = json_data
            type = HSLoadMode.Json
        result = {'type': type, 'model_name': model_name, 'data': data}
        return result


class HSBillUCService(HSHdrDtlUCService):
    # 数据校验(送审前调用)
    def validate_bill(self, id):
        print('validate_bill')

    # 送审
    def send_bill(self, id):
        print('send_to_audit')

    # 取消送审
    def un_send_bill(self, id):
        print('un_send_to_audit')

    # 审核
    def audit_bill(self, id):
        print('audit_bill')

    # 取消审核
    def un_audit_bill(self, id):
        print('un_audit_bill')

    # 关闭单据(完结)
    def close_bill(self, id):
        print('close_bill')

    # 取消关闭单据
    def un_close_bill(self, id):
        print('un_close_bill')

    # 作废单据
    def invalid_bill(self, id):
        print('invalid_bill')

    # 恢复作废单据为草稿
    def valid_bill(self, id):
        print('valid_bill')

    # 批量送审
    def send_to_audit_list(self, ids):
        print('send_to_audit_list')

    # 批量取消送审
    def un_send_to_audit_list(self, ids):
        print('un_send_to_audit_list')

    # 批量审核
    def audit_list(self, ids):
        print('audit_list')

    # 批量取消审核
    def un_audit_list(self, ids):
        print('un_audit_list')

    # 批量关闭单据(完结)
    def close_bill_list(self, ids):
        print('close_bill_list')

    # 批量取消关闭单据
    def un_close_bill_list(self, ids):
        print('un_close_bill_list')

    # 批量作废单据
    def invalid_list(self, ids):
        print('invalid_list')

    # 批量恢复作废单据为草稿
    def valid_list(self, ids):
        print('valid_list')

    # 计算单据时间戳
    def calc_bill_time_stamp(self, id):
        print('calc_bill_time_stamp')

    # 批量计算单据时间戳
    def calc_bill_list_time_stamp(self, ids):
        print('calc_bill_list_time_stamp')

    # 关闭单据明细(完结)
    def close_bill_dtl(self, id):
        print('close_bill_dtl')

    # 取消关闭单据
    def un_close_bill_dtl(self, id):
        print('un_close_bill_dtl')
