
# 测试用对比器
from huansi_utils.exception.exception import HSNotImplementError, HSException


class HSTestCompare(object):
    @property
    def success(self):
        return True, 'success'

    def _compare(self, _lamda, msg, _assert=True):
        if _lamda:
            return self.success
        else:
            return self.fail(msg, _assert)

    def fail(self, msg, _assert=True):
        if not msg.startswith('fail:'):
            msg = 'fail: ' + msg
        if _assert:
            assert False, msg
        else:
            return False, msg

    def _none(self):
        return None, None

    def exception(self, e, exception):
        '''
        异常类型对比
        :param e: 异常对象
        :param exception: 期望的异常类型
        :return: 对比结果文本
        '''
        if isinstance(e, HSNotImplementError):
            return self._none()
        elif exception:
            if isinstance(e, exception):
                return self.success
            else:
                return self.fail('期望异常[{}]但出现异常为[{}]'.format(exception.__name__, type(e).__name__), _assert=False)
        elif isinstance(e, HSException):
            return self.fail(e.message(), _assert=False)
        else:
            return self.fail(str(e), _assert=False)

    def row_count(self, row_count_expected, row_count_fact, msg=None):
        if row_count_expected == row_count_fact:
            return self.success
        msg = msg if msg else '期望数据行数[{}]但实际行数[{}]'
        return self.fail(msg.format(row_count_expected, row_count_fact))

    def _get_data_row_count(self, data):
        if isinstance(data, list):
            return len(data)
        elif 'table' in data.keys():
            return len(data['table'])
        elif isinstance(data, dict):
            return 1 if len(data)>0 else 0
        else:
            return 0

    def data_row_count(self, data, row_count_expected, msg=None):
        row_count = self._get_data_row_count(data)
        return self.row_count(row_count_expected, row_count)

    def data_exists(self, data, msg=None):
        row_count = self._get_data_row_count(data)
        self._compare(row_count > 0, '期望有数据但实际数据为空')

    def data_not_exists(self, data, msg=None):
        row_count = self._get_data_row_count(data) if data else 0
        self._compare(row_count == 0, '期望无数据但实际有数据')
