# Service基础功能类
class HSTestBaseModel(object):
    def __init__(self, session, table_name):
        self.session = session
        self.table_name = table_name

    def row_count(self, default_filter=False, where=None):
        '''
        查找指定表中的数据行数
        :param table_name: 表名
        :param default_filter: 是否按usable、bill_status筛选数据
        :param condition: 查询条件
        :return:
        '''
        sql = 'SELECT row_count=COUNT(*) FROM dbo.{} A(NOLOCK) WHERE 1=1'.format(self.table_name)
        if default_filter:
            sql = 'SELECT TOP 1 * FROM dbo.{} A(NOLOCK)'.format(self.table_name)
            data = self.session.retrive_sql(sql)
            if not data:
                return 0
            if hasattr(data, 'usable'):
                sql += 'AND A.usable=1'
            elif hasattr(data, 'bill_status'):
                sql += 'AND A.bill_status IN (2,6)'
        if where:
            sql += 'AND ({})'.format(where)
        return self.session.retrive_sql(sql).row_count

    def clear(self):
        pass

    def delete_all(self):
        sql = '''EXEC dbo.spsmMockDelete @sTableNameList='{}' 
        '''.format(self.table_name)
        self.session.exec_sql(sql)

    def delete(self, where):
        sql = '''EXEC dbo.spsmMockDelete @sTableNameList='{}',@sWhere='{}'
        '''.format(self.table_name, where.replace("'","''"))
        self.session.exec_sql(sql)

    def update(self, set, where):
        column_set = []
        for key, value in set.items():
            column_set.append('{}={}'.format(key, value))
        column_set = ','.join(column_set)
        sql = 'UPDATE A SET {} FROM dbo.{} A WHERE ({})'.format(column_set, self.table_name, where)
        self.session.exec_sql(sql)

    def insert(self, data):
        pass

    def mock_data(self, row_count=2, json_data=None, except_columns=None, table_name=None, \
                  recursion_foreign_key=True):
        return self._mock_data(row_count=row_count, json_data=json_data, except_columns=except_columns \
                               , table_name=table_name, insert=False)

    def mock_insert(self, row_count=2, json_data=None, except_columns=None, table_name=None, \
                    recursion_foreign_key=True):
        return self._mock_data(row_count=row_count, json_data=json_data, except_columns=except_columns \
                               , table_name=table_name)

    def _mock_data(self, row_count=2, json_data=None, except_columns=None,
                   table_name=None, insert=True):
        '''
        向表中插入指定行数的数据
        :param row_count: 插入行数
        :param json_data: 指定字段值的键值对
        :param columns: 需要插入的字段
        :param except_columns: 排除插入的字段
        :param insert: 是否插入数据到表中
        :return: 新插入的数据
        '''
        columns_except = ''
        columns_set = ''
        if isinstance(except_columns, str):
            columns_except = except_columns
        elif isinstance(except_columns, list):
            columns_except = ','.join(except_columns)
        if json_data:
            for key, value in json_data.items():
                columns_set += ',{}={}'.format(key, value)
            if len(columns_set) > 0:
                columns_set = columns_set[1:]
        table_name = table_name if table_name else self.table_name
        sql = '''EXEC dbo.spsmMockInsert @sTableName='{}',@iRowCount={}
        ,@sColumnExcept='{}',@sColumnSet='{}',@bInsert={},@bReturn=1 
        '''.format(table_name, row_count, columns_except, columns_set, insert)
        return self.session.query_sql(sql=sql)

    def contains_column(self, column_name, table_name=None):
        table_name = table_name if table_name else self.table_name
        sql = '''SELECT TOP 1 id=1 FROM dbo.dvColumns WHERE sTableName='{}' AND sColumnName='{}' 
        '''.format(table_name, column_name)
        data = self.session.retrive_sql(sql=sql)
        return data is not None

    def retrive(self, id=None, where=None, columns=None, except_columns=None, table_name=None):
        '''
        查询单条数据，若数据不存在，则自动mock一条数据出来
        :param where: 查询条件
        :param columns: 返回列
        :param except_columns: 排除列
        :return: 返回数据
        '''
        table_name = table_name if table_name else self.table_name
        columns = columns if columns else '*'
        condition = '1=1'
        if id:
            condition = 'id={}'.format(id)
        elif where:
            if isinstance(where, str):
                condition = where
            elif isinstance(where, dict):
                for key, value in where.items():
                    condition += " AND {}='{}' ".format(key, value)
        sql = '''SELECT TOP 1 {} FROM dbo.{} A(NOLOCK) WHERE {}
        '''.format(columns, table_name, condition)
        return self.session.retrive_sql(sql=sql)

    def query(self, where, row_count=0, columns=None, except_columns=None):
        pass

    def save(self, columns=None, except_columns=None):
        pass

    def foreign_keys(self, table_name=None):
        pass
