from huansi_utils.db.db import new_id
from huansi_utils.exception.exception import HSNotImplementError, HSArgumentError, HSValueError
from huansi_utils.test.test import HSBaseTestService


class HSBaseTableTestService(HSBaseTestService):
    '''
    单表测试基类
    '''

    def __init__(self, session=None):
        super().__init__(session=session)
        self.model = self.new_model(self.table_name)

    @property
    def table_name(self):
        return self.service.table_name

    def test__query_list(self, filter_by_usable=False):
        def test_1():
            args = {}
            row_count = self.model.row_count()
            row_count = row_count if row_count < 10 else 10
            data = self.service.query_list(args)
            self.compare.data_row_count(data, row_count)

        def test_2():
            args = {}
            self.model.delete_all()
            data = self.service.query_list(args)
            self.compare.data_not_exists(data)

        def test_3():
            args = {}
            self.model.delete_all()
            row_count = 3
            self.model.mock_insert(row_count=row_count)
            data = self.service.query_list(args)
            self.compare.data_row_count(data, row_count_expected=row_count)

        self.run_test_case(test_func=test_1)
        self.run_test_case(test_func=test_2, description='空表数据')
        self.run_test_case(test_func=test_3, description='有数据')

    def test__query_list_by_bill_id(self):
        def test_arg_none():
            data = self.service.query_list_by_bill_id(None)

        def test_1():
            data = self.model.retrive()
            if data:
                bill_id = data.bill_id
                data = self.service.query_list_by_bill_id(bill_id)
                self.compare.data_exists(data)

        def test_2():
            data = self.service.query_list_by_bill_id(0)
            self.compare.data_not_exists(data)

        def test_3():
            row_count = 3
            self.model.delete_all()
            data = self.model.mock_insert(row_count=row_count)
            bill_id = data[0].bill_id
            data = self.service.query_list_by_bill_id(bill_id)
            self.compare.data_row_count(data=data, row_count_expected=row_count)

        if not self.model.contains_column('bill_id'):
            raise HSNotImplementError()
        self.run_test_case(test_arg_none, exception=HSArgumentError)
        self.run_test_case(test_1)
        self.run_test_case(test_2, '空表数据')
        self.run_test_case(test_3, '多行数据')

    def test__query_one(self):
        def test_arg_none():
            id = None
            data = self.service.query_one(id)

        def test_1():
            data = self.model.retrive()
            if data:
                id = data.id
                data = self.service.query_one(id)
                self.compare.data_exists(data)

        def test_2():
            data = self.service.query_one(0)
            self.compare.data_not_exists(data)

        def test_3():
            id = new_id()
            data = {'id': id}
            self.model.delete_all()
            row_count = 1
            self.model.mock_insert(row_count=row_count, json_data=data)
            data = self.service.query_one(id)
            self.compare.data_row_count(data=data, row_count_expected=row_count)

        self.run_test_case(test_arg_none, exception=HSArgumentError)
        self.run_test_case(test_1)
        self.run_test_case(test_2, '空表数据')
        self.run_test_case(test_3, '多行数据')

    def test__delete_list(self):
        def test_arg_none():
            data = self.service.delete_list(None)

        self.run_test_case(test_arg_none, exception=HSArgumentError)

    def test__delete_one(self):
        def test_arg_none():
            data = self.service.delete_one(None)

        def test_1():
            data = self.model.retrive()
            if data:
                id = data.id
                self.service.delete_one(id)
                data = self.model.retrive(id=id)
                self.compare.data_not_exists(data)

        def test_2():
            self.model.delete_all()
            id = new_id()
            self.model.mock_insert(row_count=1, json_data={'id': id})
            self.service.delete_one(id)
            data = self.model.retrive(id=id)
            self.compare.data_not_exists(data)

        self.run_test_case(test_arg_none, exception=HSArgumentError)
        self.run_test_case(test_1)
        self.run_test_case(test_2)

    def test__save_list(self):
        pass

    def _test_data_save_one(self):
        return {
            'not_null_fields': 'bill_date',
            'foreign_keys': 'bill_id=im_arrive.id,customer_id=pb_customer.id',
            'ref': 'bill_status=292',
            'unique': 'bill_no,company_id+iType',
        }

    def test__save_one(self):
        def _get_not_null_fields():
            data = self._test_data_save_one()
            if not data:
                raise HSNotImplementError()
            not_null_fields = data['not_null_fields']
            if not not_null_fields:
                raise HSNotImplementError()
            return not_null_fields

        def _run_save_one(data, exception=None):
            self.begin_trans()
            try:
                self.service.save_one(json_data=data)
                self.rollback_trans()
                # self.compare.exception(e=None, exception=exception)
            except Exception as e:
                self.rollback_trans()
                raise
                # self.compare.exception(e=e, exception=exception)

        def test_insert():
            '''
            测试新增
            '''
            mock_data = dict(self.model.mock_data(row_count=1)[0])
            _run_save_one(mock_data)

        def test_update():
            '''
            测试更新
            :return:
            '''
            mock_data = dict(self.model.mock_insert(row_count=1)[0])
            _run_save_one(mock_data)

        def test_insert_not_null_fields():
            '''
            测试非空字段（要求前端一定要传入的字段）
            '''
            not_null_fields = _get_not_null_fields()
            data = self.model.mock_data()
            line = dict(data[0])
            for field in not_null_fields.split(','):
                value = line[field]
                line[field] = None
                _run_save_one(line, HSValueError)
                line[field] = value

        def test_update_not_null_fields():
            '''
            测试非空字段（要求前端一定要传入的字段）
            '''
            not_null_fields = _get_not_null_fields()
            data = self.model.mock_insert()
            for field in not_null_fields.split(','):
                value = data[field]
                data[field] = None
                _run_save_one(data, HSValueError)
                data[field] = value

        def test_foreign_key():
            foreign_keys = self.model.foreign_keys()
            if not foreign_keys:
                raise HSNotImplementError()
            data = self.model.mock_data()
            for line in foreign_keys:
                field = line.sforeignkey
                value = data[field]
                data[field] = new_id()
                _run_save_one(data, HSValueError)
                data[field] = value

        def test_ref_data():
            ref_fields = _get_not_null_fields()
            if not ref_fields:
                raise HSNotImplementError()
            data = self.model.mock_insert()
            for field in ref_fields.split(','):
                value = data[field]
                data[field] = new_id()
                _run_save_one(data, HSValueError)
                data[field] = value

        def test_ignore_insert_field(self):
            '''
            测试忽略插入字段，如create_time
            '''
            pass

        def test_ignore_update_field(self):
            pass

        def test_unique_field(self):
            fields = _get_not_null_fields()
            if not fields:
                raise HSNotImplementError()
            data = self.model.mock_data(row_count=3)
            for columns in fields.split(','):
                for field in columns.split('+'):
                    data[1][field] = data[0][field]
                _run_save_one(data[0])
                _run_save_one(data[1], HSValueError)
                for field in columns.split('+'):
                    data[1][field] = data[2][field]

        self.run_test_case(test_func=test_insert)
        self.run_test_case(test_func=test_update)
        # self.run_test_case(test_func=test_insert_not_null_fields)
        # self.run_test_case(test_func=test_update_not_null_fields)
        # self.run_test_case(test_func=test_foreign_key)
        # self.run_test_case(test_func=test_ref_data)
        # self.run_test_case(test_func=test_unique_field)
        # self.run_test_case(test_func=test_ignore_insert_field)
        # self.run_test_case(test_func=test_ignore_update_field)

    def test___validate(self):
        pass
