import datetime
from huansi_utils.db.db import new_session
from huansi_utils.exception.exception import HSNotImplementError
from huansi_utils.server.server import HSBaseService
from huansi_utils.test.model import HSTestBaseModel
from huansi_utils.test.compare import HSTestCompare
from flask import jsonify


# Service基础功能类
class HSBaseTestService(HSBaseService):
    def __init__(self, session=None):
        self.session = session if session else new_session()
        self.session._total_rollback_count = -1
        self.service = self.new_service()
        self.compare = HSTestCompare()
        self.test_result = {}
        self._raise = False
        self.show_all = False

    def service_class(self):
        return None

    def new_service(self):
        cls = self.service_class()
        obj = cls.__new__(cls)
        obj.session = self.session
        obj._test_mode = True
        return obj

    def new_model(self, table_name=None):
        if not table_name:
            table_name = self.service.table_name
        return HSTestBaseModel(session=self.session, table_name=table_name)

    def def_test_methods(self):
        result = []
        for method in dir(self):
            if method.startswith('test__'):
                result.append(getattr(self, method))
        return result

    def run(self, _jsonify=True, _raise=False, show_all=False):
        self._raise = _raise
        self.show_all = show_all
        self.session.begin_trans()
        try:
            test_methods = self.def_test_methods()
            response = {}
            response['_test_class'] = self.__class__.__name__
            response['_test_time'] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            a = datetime.datetime.now()
            for method in test_methods:
                try:
                    name = method.__name__
                    if name.startswith('test__'):
                        name = name[6:]
                    self.test_result = {}
                    method()
                    result = self.test_result if len(self.test_result) > 0 else 'success'
                    if show_all:
                        response[name] = result
                except Exception as e:
                    if isinstance(e, HSNotImplementError):
                        if self.show_all:
                            response[name] = None
                    else:
                        response[name] = 'fail:' + str(e)
                    if self._raise:
                        raise
            self.session.rollback_trans()
            print(response)
            response['_test_seconds'] = (datetime.datetime.now() - a).seconds
            # response = 'test success\n'+'\n'.join(response)
            return jsonify(response) if _jsonify else response
        except Exception as e:
            self.session.rollback_trans()
            print('测试失败：', e)
            raise

    def run_test_case(self, test_func, description=None, pre_func=None, exception=None):
        '''
        单个测试用例执行
        :param test_func: 测试方法
        :param pre_func: 前处理方法
        :param exception: 期望的异常类型
        :return: 测试方法是否成功
        '''
        func_name = test_func.__name__
        if description:
            func_name += ':  {}'.format(description)
        self.begin_trans()
        try:
            if pre_func:
                pre_func()
            test_func()
            self.rollback_trans()
            if self.show_all:
                result, self.test_result[func_name] = self.compare.success
            else:
                result = True
        except Exception as e:
            self.rollback_trans()
            if isinstance(e, HSNotImplementError):
                raise
            result, self.test_result[func_name] = self.compare.exception(e, exception)
            if self._raise:
                raise

    def mock_data(self, table_name, row_count):
        pass

    def equal(self, data1, data2):
        pass
