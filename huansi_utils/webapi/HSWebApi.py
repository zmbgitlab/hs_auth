import json
import traceback
from collections import Iterable
from functools import wraps
from urllib.parse import quote

from flask import g, make_response, jsonify
from sqlalchemy.engine import RowProxy
from werkzeug.wrappers import Response as ResponseBase

from huansi_utils.common.json import model_to_json
from huansi_utils.common.schema import schema_data
from huansi_utils.db.model import HSBaseModel
from huansi_utils.exception.exception import HSMessage
from huansi_utils.exception.message import logger
from huansi_utils.webapi import WebApi


class HSWebApi(WebApi):
    @staticmethod
    def _get_prefix(cls, prefix):
        # 强制前缀小写
        return WebApi._get_prefix(cls, prefix).lower()

    def _handle_func_url(self, url):
        url = super()._handle_func_url(url)
        if not url.endswith('/'):
            url += '/'
        # 强制函数的URL小写
        return url.lower()

    @staticmethod
    def set_g(session):
        from flask import request
        # session
        g.hs_session = session
        # 登录的用户信息
        g.user_info = json.loads(request.headers.environ.get('HTTP_USER_INFO', '{}'), encoding='utf8')
        g.debug_sql = request.headers.environ.get('HTTP_DEBUG_SQL', 'false')
        # 记录的sql语句
        g.sql = []
        if g.debug_sql.lower() == 'true':
            session.db_session.bind.logger.echo = True
        if g.user_info:
            user_id = 0
            if isinstance(g.user_info.get('user_id'), int):
                user_id = g.user_info.get('user_id')
            company_id = g.user_info.get('company_id')
            lang = g.user_info.get('lang')
            session.exec_sql('''IF EXISTS (SELECT TOP 1 1 FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[sppbSetSessionVar]')
	AND OBJECTPROPERTY(id, N'IsProcedure') = 1)
BEGIN
	EXEC dbo.sppbSetSessionVar @iUserId=:user_id
		,@iCompanyId=:company_id
		,@sLanguage=:lang
		,@sWebURL=:web_url
		,@sApiURL=:api_url
END''', user_id=user_id, company_id=company_id, lang=lang, web_url='', api_url=request.url)

    def output(self, resource):
        """Wraps a resource (as a flask view function), for cases where the
        resource does not directly return a response object

        :param resource: The resource as a flask view function
        """

        @wraps(resource)
        def wrapper(*args, **kwargs):

            # 对请求部分做了改动
            resp = None
            try:
                from huansi_utils.db.db import new_session
                session = new_session(begin=True)
                self.set_g(session)
                resp = resource(*args, **kwargs)
                session.commit_trans()
            except Exception as e:
                session.rollback_trans()
                logger.error('{}\n{}'.format(e, traceback.format_exc()))
                # 写入db
                HSMessage(e).insert_error_db(session)
                # if __debug__:
                #     raise e
                resp = self._error_message(e)
            finally:
                session.close()

            if isinstance(resp, ResponseBase):  # There may be a better way to test
                return resp
            data, code, headers = self.unpack(resp)
            # 处理环思返回结果,这里的data就是ResponseBase对象,不需要最任何处理
            # 直接返回即可
            if isinstance(data, ResponseBase):  # There may be a better way to test
                return data
            return self.make_response(data, code, headers=headers)

        return wrapper

    @staticmethod
    def output_json(data, code, headers=None):
        resp = make_response(data if isinstance(data, str) else jsonify(HSWebApi._to_serialize_object(data)), code)
        resp.headers.extend(headers or {})
        # sql调试模式返回sql语句
        if g.debug_sql.lower() == 'true':
            sql = {'debug_sql': quote('|||||'.join(g.sql).replace("\n", '***').replace("\r", ''))}
            # sql = {'debug_sql': '|||||'.join(g.sql).replace("\n", '***').replace("\r", '')}
            resp.headers.extend(sql)
        g.hs_session.db_session.bind.logger.echo = False
        return resp

    @staticmethod
    def _to_serialize_object(data):
        if isinstance(data, dict) or isinstance(data, str):
            return data
        many = False
        if isinstance(data, Iterable) and any(data):
            first = data[0]
            if isinstance(first, dict):
                return data
            else:
                many = isinstance(first, RowProxy) or isinstance(first, schema_data) or isinstance(first, HSBaseModel)
        data = model_to_json(data, many=many)
        return data

    @staticmethod
    def _error_message(e):
        # logger.debug(e)
        error_message, error_code = HSMessage(e).format_message()
        logger.error(error_message)
        if error_code == 801:
            error_code = 200
        # logger.debug(error_message)
        return error_message, error_code
