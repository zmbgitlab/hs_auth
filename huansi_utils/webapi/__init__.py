from functools import wraps
from inspect import isfunction

from flask.helpers import get_root_path, make_response
from flask.views import MethodView
from flask import request, Blueprint, current_app
from collections import namedtuple, OrderedDict

from werkzeug.exceptions import NotAcceptable, InternalServerError
from werkzeug.wrappers import Response as ResponseBase
from flask import make_response as original_flask_make_response

__all__ = ('WebApi', 'ApiController')

ControllerInfo = namedtuple("controllerInfo", ["view_func", "prefix"])


class WebApi(object):

    def __init__(self, name, import_name, app=None, prefix=None, root_path=None):
        if not name:
            raise ValueError("name invalid")
        if not import_name:
            raise ValueError("import_name invalid")
        self.name = name
        self.import_name = import_name
        if root_path is None:
            root_path = get_root_path(self.import_name)

        self.root_path = root_path
        self.representations = OrderedDict([('application/json', self.output_json)])
        self.prefix = prefix
        self.controllers = dict()
        self.module_url_funcs = dict()
        self.blueprint = None
        self.app = None
        self.default_mediatype = 'application/json'
        self.app = app

        if isinstance(app, Blueprint):
            self.blueprint = app

    @staticmethod
    def output_json(data, code, headers=None):
        """Makes a Flask response with a JSON encoded body"""

        settings = current_app.config.get('RESTFUL_JSON', {})

        # If we're in debug mode, and the indent is not set, we set it to a
        # reasonable value here.  Note that this won't override any existing value
        # that was set.  We also set the "sort_keys" value.
        if current_app.debug:
            settings.setdefault('indent', 4)

        # always end the json dumps with a new line
        # see https://github.com/mitsuhiko/flask/pull/1262
        from json import dumps
        dumped = dumps(data, **settings) + "\n"

        resp = make_response(dumped, code)
        resp.headers.extend(headers or {})
        return resp

    def init_app(self, app):
        """
        初始化app,负责把路由注册到app里
        :param app:
        :return:
        """
        self.app = app
        if isinstance(app, Blueprint):
            self.blueprint = app
        self._flush_api_route_cache()

    def _flush_api_route_cache(self):
        """
        把缓存的路由信息注册到路由
        :return:
        """
        if self.app:
            for module_name, controller_info in self.controllers.items():
                self._register_route_single_module(controller_info, self.module_url_funcs.get(module_name, dict()))

    def _register_api(self, cls, prefix=None):
        """
        注册API路由,如果app为None,则写入缓存,否则直接注册
        :param cls:
        :param prefix:
        :return:
        """
        class_name = cls.__module__ + "." + cls.__name__
        if self.controllers.get(class_name):
            return
        info = ControllerInfo(view_func=self.output(cls.as_view(class_name.replace(".", "_"))),
                              prefix=self._get_prefix(cls, prefix))
        self.controllers[class_name] = info
        if self.app:
            self._register_route_single_module(info, self.module_url_funcs.get(class_name, dict()))

    def output(self, resource):
        """Wraps a resource (as a flask view function), for cases where the
        resource does not directly return a response object

        :param resource: The resource as a flask view function
        """

        @wraps(resource)
        def wrapper(*args, **kwargs):
            resp = resource(*args, **kwargs)
            if isinstance(resp, ResponseBase):  # There may be a better way to test
                return resp
            data, code, headers = self.unpack(resp)
            # 处理环思返回结果,这里的data就是ResponseBase对象,不需要最任何处理
            # 直接返回即可
            if isinstance(data, ResponseBase):  # There may be a better way to test
                return data
            return self.make_response(data, code, headers=headers)

        return wrapper

    @staticmethod
    def unpack(value):
        """Return a three tuple of data, code, and headers"""
        if not isinstance(value, tuple):
            return value, 200, {}

        try:
            data, code, headers = value
            return data, code, headers
        except ValueError:
            pass

        try:
            data, code = value
            return data, code, {}
        except ValueError:
            pass

        return value, 200, {}

    def make_response(self, data, *args, **kwargs):
        """Looks up the representation transformer for the requested media
        type, invoking the transformer to create a response object. This
        defaults to default_mediatype if no transformer is found for the
        requested mediatype. If default_mediatype is None, a 406 Not
        Acceptable response will be sent as per RFC 2616 section 14.1

        :param data: Python object containing response data to be transformed
        """
        default_mediatype = kwargs.pop('fallback_mediatype', None) or self.default_mediatype
        mediatype = request.accept_mimetypes.best_match(
            self.representations,
            default=default_mediatype,
        )
        if mediatype is None:
            raise NotAcceptable()
        if mediatype in self.representations:
            resp = self.representations[mediatype](data, *args, **kwargs)
            resp.headers['Content-Type'] = mediatype
            return resp
        elif mediatype == 'text/plain':
            resp = original_flask_make_response(str(data), *args, **kwargs)
            resp.headers['Content-Type'] = 'text/plain'
            return resp
        else:
            raise InternalServerError()

    def _get_endpoint(self, prefix, url):
        """
        获取当前API的endpoint,注意,返回的endpoint不一定是最终运行的,因为还有buleprint等会直接影响在app里注册的endpoint
        :param prefix: api类的前缀
        :param url: api的url
        :return: 当前api的endpoint
        """
        if self.prefix:
            return "/".join([self.name, self.prefix.lstrip("/"), prefix.lstrip("/"), url.lstrip("/")]).replace(".", "_")
        return "/".join([self.name, prefix.lstrip("/"), url.lstrip("/")]).replace(".", "_")

    def _get_full_endpoint(self, endpoint):
        """
        获取完整的endpoint,主要是处理有buleprint的情况
        :param endpoint:当前api的endpoint
        :return:一个完整的endpoint
        """
        if self.blueprint:
            endpoint = self.blueprint.name + '.' + endpoint
        return endpoint

    def _get_rule(self, prefix, url):
        """
        获取包括Api对象前缀在内的完整路由
        :param prefix:
        :param url:
        :return:
        """

        def _get_url():
            if self.prefix:
                return self.prefix + prefix + url
            return prefix + url

        rule = _get_url()
        if not rule.startswith("/"):
            rule = "/" + rule
        return rule

    def _add_url_rule(self, view_func, prefix, url, methods=["GET", "POST", "PUT", "DELETE"]):
        """
        添加到app或blueprint的路由系统
        :param view_func:apicontroller子类对象按flask规范转成的funcview
        :param prefix:api前缀
        :param url:方法对应的url
        :param methods:此url对应的所有方法
        :return:
        """
        rule = self._get_rule(prefix, url)
        endpoint = self._get_endpoint(prefix, url)
        if self.blueprint:
            self.blueprint.add_url_rule(rule=rule, endpoint=endpoint, view_func=view_func,
                                        methods=methods)
        else:
            self.app.add_url_rule(rule=rule, view_func=view_func, endpoint=endpoint, methods=methods)

    def _register_route_single_module(self, controller_info, url_methods):
        """
        注册单个模块的路由
        :param controller_info:模块的前缀及view_func
        :param url_methods:模块的所有url->方法集合 映射
        :return:
        """
        if not url_methods:
            # 如果没有为方法单独注册,那么按Flask默认规则来,也就是函数名=httpmethod.lower()
            self._add_url_rule(view_func=controller_info.view_func, url="", prefix=controller_info.prefix)
            return
        for url, funcs in url_methods.items():
            # key=url,value=方法名列表
            methods = set(self._get_http_methods(funcs))
            self._add_url_rule(prefix=controller_info.prefix, url=url, view_func=controller_info.view_func,
                               methods=methods)
            # 写入apicontroller实例对象内的规则列表,rules里的endpoint为key,此endpoint是完整的endpoint
            controller_info.view_func.view_class.append_rule(
                self._get_full_endpoint(self._get_endpoint(controller_info.prefix, url)), funcs)

    def _get_http_methods(self, funcs):
        """
        根据方法名,返回对应的HTTP Method
        :param funcs:
        :return:
        """
        for func in funcs:
            yield self._get_http_method(func)

    @staticmethod
    def _get_prefix(cls, prefix):
        """
        获取前缀
        :param cls:
        :param prefix:
        :return:
        """
        prefix = prefix or cls.__name__.lower()
        if not prefix.startswith("/"):
            prefix = "/{}".format(prefix)
        if prefix.endswith("/"):
            prefix = prefix[:-1]
        return prefix

    def _handle_func_url(self, url):
        if url is None:
            url = ""
        if url and not url.startswith("/"):
            url = "/" + url
        return url

    def _cache_func_route(self, func, url=None):
        """
        将方法的路由写入缓存,此时并不写入Flask
        :param func:函数
        :param url:函数url
        :return:
        """

        # 由于函数装饰器在类装饰器前执行,所以此时并拿不到类对应的prefix和as_view得到的viewmode方法
        class_name = func.__module__ + "." + func.__qualname__.split(".")[0]
        url = self._handle_func_url(url)
        # 先添加到自己的列表,后面再执行注入
        # 3层集合,module为字典,取到url对应的方法列表添加
        self.module_url_funcs.setdefault(class_name, dict()).setdefault(url, []).append(func.__name__)

    @staticmethod
    def _get_http_method(func_name):
        if func_name.startswith("get"):
            return "GET"
        if func_name.startswith("post"):
            return "POST"
        if func_name.startswith("put"):
            return "PUT"
        if func_name.startswith("delete") or func_name.startswith("del"):
            return "DELETE"
        return "GET"

    def api(self, url=None):
        """
        装饰一个ApiController或其实例函数,可以设置此Api的url,如果不设置,默认取类名
        :param url:api的访问前置,默认取api的类名
        :return:
        """

        def decorator(cls_or_func):
            if isfunction(cls_or_func):
                self._cache_func_route(cls_or_func, url=url)
                return cls_or_func
            if issubclass(cls_or_func, ApiController):
                self._register_api(cls_or_func, url)
                return cls_or_func
            else:
                raise ValueError("route装饰器仅能用在ApiController子类及其子类的函数")

        return decorator


class ApiController(MethodView):
    __method_decorators = dict()
    # rules为endpoint为key,value为对应的方法集合,其中endpoint为完整的endpoint,如果有buleprint,则包含buleprint部分
    __rules = dict()

    @classmethod
    def append_method_decorator(cls, decorator):
        cls.__method_decorators.setdefault(cls, []).append(decorator)

    @classmethod
    def append_rule(cls, k,v):
        cls.__rules.setdefault(cls, dict())[k]=v

    @property
    def rules(self):
        return self.__rules.get(self.__class__, [])

    @property
    def method_decorators(self):
        return self.__method_decorators.get(self.__class__, [])

    def dispatch_request(self, *args, **kwargs):
        http_method = request.method.lower()
        functions = self.rules.get(request.endpoint, [])
        functions = functions if len(functions) == 1 else list(
            filter(lambda name: name.startswith(http_method), functions))
        if functions:
            func_name = functions[0]
            method = getattr(self, func_name, None)

            assert method is not None, 'Unimplemented method %r' % request.method

            for decorator in self.method_decorators:
                method = decorator(method)

            return method(*args, **kwargs)
        else:
            return super().dispatch_request(*args, **kwargs)
