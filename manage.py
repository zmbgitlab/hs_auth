from flask_cors import CORS
from flask_app import global_app

from huansi_utils.route import _route_register_instance

_route_register_instance.init_app(global_app)

# 允许跨域请求
CORS(global_app, supports_credentials=True)

'''新的一套API运行方式'''
if __name__ == '__main__':
    global_app.run(host="0.0.0.0", port=int(global_app.config.get('IP_PORT', 5050)), debug=True)
