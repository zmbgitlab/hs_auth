#!/usr/bin/python
# -*- coding: utf-8 -*-
from flask_app import global_app as instance
from werkzeug.contrib.fixers import ProxyFix


app = ProxyFix(instance)
